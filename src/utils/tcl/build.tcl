#-------------------------------------------------------------------------------
# file:      build.tcl
# author:    Michael Wurm <wurm.michael95@gmail.com>
# copyright: 2017-2019 Michael Wurm
# brief:     Opens Altera Project and runs synthesis.
#-------------------------------------------------------------------------------

# Load Quartus Prime Tcl Project package
package require ::quartus::project
load_package flow

set project_file [lindex $argv 0]

if {[project_exists $project_file]} {
  project_open -revision project_soc $project_file
} else {
  puts "(MWURM) ERROR: Project file '$project_file' not found."
  exit 1
}

if {[catch {
  execute_flow -full_compile
} ]} {
  puts "(MWURM) ERROR: Compiling of project failed."
  exit 1
}

puts "(MWURM) Done."
exit 0
