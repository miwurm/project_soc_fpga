#-------------------------------------------------------------------------------
# file:      ip_compile.tcl
# author:    Michael Wurm <wurm.michael95@gmail.com>
# copyright: 2017-2019 Michael Wurm
# brief:     Compiles IP files into respective work libraries.
#-------------------------------------------------------------------------------

eval transcript quietly
eval onerror {quit -f}
eval onbreak {quit -f}

# Set compiler flags
set param "-fsmverbose btw"
set vhdl_version "-2008"

# Create and map work library
set work_lib "blinkylightlib"
eval vlib $work_lib
eval vmap $work_lib $work_lib

set root_dir "../.."
set ipsrc $root_dir/src/utils/tcl/ip_src.txt

# Compile all files
if {[file isfile $ipsrc]} {
    set fp [open $ipsrc r]
    while {[gets $fp line] >= 0} {
        scan $line "%s" filename
        eval echo $line
        eval echo $filename
        if {[string match "*.vhd" $filename]} {
            eval vcom $param $vhdl_version -work $work_lib $root_dir/$filename
        }
    }
}

eval quit -f
