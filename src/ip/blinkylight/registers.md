# BlinkyLight Register Map

LED and Sevensegment control for DE1-SoC Board.

Register width: 32 bits<br>
Address width: 7 bits

---
### STATUS
Status of BlinkyLight.

| Address: `0x0` | *READ_ONLY* |
| :--- | ---: |
| *KEYS*<br>Is set when any user key is pressed. | Bits: `[0]`<br>Reset: `0x0` |
| *FPGA_RUNNING*<br>Signalize that FPGA is running. | Bits: `[1]`<br>Reset: `0x0` |

---
### GUI_CONTROL
Controls for LEDs and Sevensegments.

| Address: `0x4` | *WRITE_ONLY* |
| :--- | ---: |
| *UPDATE_ENABLE*<br>Updates LEDs and Sevensegments when enabled. | Bits: `[0]`<br>Reset: `0x0` |
| *BLANK_SEVSEG*<br>Blanks Sevensegments. | Bits: `[1]`<br>Reset: `0x0` |

---
### LED_DIMMVALUE0
Brightness of LED0.

| Address: `0x8` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[8:0]`<br>Reset: `0x0` |

---
### LED_DIMMVALUE1
Brightness of LED1.

| Address: `0xC` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[8:0]`<br>Reset: `0x0` |

---
### LED_DIMMVALUE2
Brightness of LED2.

| Address: `0x10` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[8:0]`<br>Reset: `0x0` |

---
### LED_DIMMVALUE3
Brightness of LED3.

| Address: `0x14` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[8:0]`<br>Reset: `0x0` |

---
### LED_DIMMVALUE4
Brightness of LED4.

| Address: `0x18` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[8:0]`<br>Reset: `0x0` |

---
### LED_DIMMVALUE5
Brightness of LED5.

| Address: `0x1C` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[8:0]`<br>Reset: `0x0` |

---
### LED_DIMMVALUE6
Brightness of LED6.

| Address: `0x20` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[8:0]`<br>Reset: `0x0` |

---
### LED_DIMMVALUE7
Brightness of LED7.

| Address: `0x24` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[8:0]`<br>Reset: `0x0` |

---
### SEV_SEGMENT_DISPLAY0
Character value to be displayed on sevensegments 0.

| Address: `0x28` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[4:0]`<br>Reset: `0x11` |

---
### SEV_SEGMENT_DISPLAY1
Character value to be displayed on sevensegments 1.

| Address: `0x2C` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[4:0]`<br>Reset: `0x11` |

---
### SEV_SEGMENT_DISPLAY2
Character value to be displayed on sevensegments 2.

| Address: `0x30` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[4:0]`<br>Reset: `0x11` |

---
### SEV_SEGMENT_DISPLAY3
Character value to be displayed on sevensegments 3.

| Address: `0x34` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[4:0]`<br>Reset: `0x11` |

---
### SEV_SEGMENT_DISPLAY4
Character value to be displayed on sevensegments 4.

| Address: `0x38` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[4:0]`<br>Reset: `0x11` |

---
### SEV_SEGMENT_DISPLAY5
Character value to be displayed on sevensegments 5.

| Address: `0x3C` | *READ_WRITE* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[4:0]`<br>Reset: `0x11` |

---
### MAGIC_VALUE
Magic constant to identity Blinkylight.

| Address: `0x40` | *READ_ONLY* |
| :--- | ---: |
| *VALUE*<br> | Bits: `[31:0]`<br>Reset: `0x4711ABCD` |

---
### IRQS
Interrupt register (W1C)

| Address: `0x44` | *INTERRUPT* |
| :--- | ---: |
| *KEY*<br>A user key was pressed. | Bits: `[0]`<br>Reset: `0x0` |
| *PPS*<br>Pulse per second. | Bits: `[1]`<br>Reset: `0x0` |

---
### IRQ_ERRORS
Interrupt error register (W1C)

| Address: `0x48` | *INTERRUPT_ERROR* |
| :--- | ---: |
| *KEY*<br>IRQS.KEY triggered twice without clearing. | Bits: `[0]`<br>Reset: `0x0` |
| *PPS*<br>IRQS.PPS triggered twice without clearing. | Bits: `[1]`<br>Reset: `0x0` |


---
*Copyright (C) 2017-2019 Michael Wurm*