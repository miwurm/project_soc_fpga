-------------------------------------------------------------------------------
--! @file      blinkylight_top.vhd
--! @author    Michael Wurm <wurm.michael95@gmail.com>
--! @copyright 2017-2019 Michael Wurm
--! @brief     BlinkyLight top-level wrapper.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library blinkylightlib;
use blinkylightlib.blinkylight_pkg.all;

--! @brief Entity declaration of blinkylight_top
--! @details
--! A top-level wrapper for the BlinkyLight implementation.
--!
--! **NOTE:**
--! Some development environments suggest std_logic as the port type for IP
--! top-level components. However, std_ulogic is preferred in the actual
--! implementation as it helps catch 'multiple driver errors' during simulation.

entity blinkylight_top is
  generic (
    is_simulation_g : boolean := true;
    --! Build Avalon MM Interface
    avalon_mm_inc_g : boolean := true;
    --! Build AXI4-Lite Interface
    axi4_lite_inc_g : boolean := true);
  port (
    --! @name Clocks and resets
    --! @{

    --! System clock
    clk_i   : in std_logic;
    --! Asynchronous reset
    rst_n_i : in std_logic;

    --! @}
    --! @name User interface hardware
    --! @{

    key_i    : in  std_logic_vector(num_of_keys_c-1 downto 0);
    switch_i : in  std_logic_vector(num_of_switches_c-1 downto 0);
    led_o    : out std_logic_vector(num_of_leds_c-1 downto 0);
    hex0_o   : out std_logic_vector(6 downto 0);
    hex1_o   : out std_logic_vector(6 downto 0);
    hex2_o   : out std_logic_vector(6 downto 0);
    hex3_o   : out std_logic_vector(6 downto 0);
    hex4_o   : out std_logic_vector(6 downto 0);
    hex5_o   : out std_logic_vector(6 downto 0);

    --! @}
    --! @name Interrupts
    --! @{

    blinky_irq_o : out std_logic;
    blinky_pps_o : out std_logic;

    --! @}
    --! @name Status
    --! @{

    running_o : out std_logic;

    --! @}
    --! @name Avalon MM register interface
    --! @{

    s1_address_i       : in  std_logic_vector(reg_if_addr_width_c-1 downto 0);
    s1_write_i         : in  std_logic;
    s1_writedata_i     : in  std_logic_vector(31 downto 0);
    s1_read_i          : in  std_logic;
    s1_readdata_o      : out std_logic_vector(31 downto 0);
    s1_readdatavalid_o : out std_logic;
    s1_response_o      : out std_logic_vector(1 downto 0);

    --! @}
    --! @name AXI-Lite register interface
    --! @{

    s_axi_awaddr_i  : in  std_logic_vector(reg_if_addr_width_c-1 downto 0);
    s_axi_awprot_i  : in  std_logic_vector(2 downto 0);
    s_axi_awvalid_i : in  std_logic;
    s_axi_awready_o : out std_logic;
    s_axi_wdata_i   : in  std_logic_vector(31 downto 0);
    s_axi_wstrb_i   : in  std_logic_vector(3 downto 0);
    s_axi_wvalid_i  : in  std_logic;
    s_axi_wready_o  : out std_logic;
    s_axi_bresp_o   : out std_logic_vector(1 downto 0);
    s_axi_bvalid_o  : out std_logic;
    s_axi_bready_i  : in  std_logic;
    s_axi_araddr_i  : in  std_logic_vector(reg_if_addr_width_c-1 downto 0);
    s_axi_arprot_i  : in  std_logic_vector(2 downto 0);
    s_axi_arvalid_i : in  std_logic;
    s_axi_arready_o : out std_logic;
    s_axi_rdata_o   : out std_logic_vector(31 downto 0);
    s_axi_rresp_o   : out std_logic_vector(1 downto 0);
    s_axi_rvalid_o  : out std_logic;
    s_axi_rready_i  : in  std_logic);

  --! @}

end entity blinkylight_top;

--! Runtime configurable RTL implementation of blinkylight_top
architecture rtl of blinkylight_top is

begin  -- architecture rtl

  -----------------------------------------------------------------------------
  -- Instantiations
  -----------------------------------------------------------------------------

  blinkylight_inst : entity blinkylightlib.blinkylight
    generic map (
      is_simulation_g => is_simulation_g,
      avalon_mm_inc_g => true,
      axi4_lite_inc_g => true)
    port map (
      clk_i   => std_ulogic(clk_i),
      rst_n_i => std_ulogic(rst_n_i),

      key_i                     => std_ulogic_vector(key_i),
      switch_i                  => std_ulogic_vector(switch_i),
      std_ulogic_vector(led_o)  => led_o,
      std_ulogic_vector(hex0_o) => hex0_o,
      std_ulogic_vector(hex1_o) => hex1_o,
      std_ulogic_vector(hex2_o) => hex2_o,
      std_ulogic_vector(hex3_o) => hex3_o,
      std_ulogic_vector(hex4_o) => hex4_o,
      std_ulogic_vector(hex5_o) => hex5_o,

      std_ulogic(blinky_irq_o) => blinky_irq_o,
      std_ulogic(blinky_pps_o) => blinky_pps_o,
      std_ulogic(running_o)    => running_o,

      s1_address_i                     => std_ulogic_vector(s1_address_i),
      s1_write_i                       => std_ulogic(s1_write_i),
      s1_writedata_i                   => std_ulogic_vector(s1_writedata_i),
      s1_read_i                        => std_ulogic(s1_read_i),
      std_ulogic_vector(s1_readdata_o) => s1_readdata_o,
      std_ulogic(s1_readdatavalid_o)   => s1_readdatavalid_o,
      std_ulogic_vector(s1_response_o) => s1_response_o,

      s_axi_awaddr_i                  => std_ulogic_vector(s_axi_awaddr_i),
      s_axi_awprot_i                  => std_ulogic_vector(s_axi_awprot_i),
      s_axi_awvalid_i                 => std_ulogic(s_axi_awvalid_i),
      std_logic(s_axi_awready_o)      => s_axi_awready_o,
      s_axi_wdata_i                   => std_ulogic_vector(s_axi_wdata_i),
      s_axi_wstrb_i                   => std_ulogic_vector(s_axi_wstrb_i),
      s_axi_wvalid_i                  => std_ulogic(s_axi_wvalid_i),
      std_logic(s_axi_wready_o)       => s_axi_wready_o,
      std_logic_vector(s_axi_bresp_o) => s_axi_bresp_o,
      std_logic(s_axi_bvalid_o)       => s_axi_bvalid_o,
      s_axi_bready_i                  => std_ulogic(s_axi_bready_i),
      s_axi_araddr_i                  => std_ulogic_vector(s_axi_araddr_i),
      s_axi_arprot_i                  => std_ulogic_vector(s_axi_arprot_i),
      s_axi_arvalid_i                 => std_ulogic(s_axi_arvalid_i),
      std_logic(s_axi_arready_o)      => s_axi_arready_o,
      std_logic_vector(s_axi_rdata_o) => s_axi_rdata_o,
      std_logic_vector(s_axi_rresp_o) => s_axi_rresp_o,
      std_logic(s_axi_rvalid_o)       => s_axi_rvalid_o,
      s_axi_rready_i                  => std_ulogic(s_axi_rready_i));

end architecture rtl;
