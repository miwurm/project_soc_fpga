/*
 * Copyright (C) 2017-2019 Michael Wurm
 * Author: Super Easy Register Scripting Engine (SERSE)
 *
/* BlinkyLight register map */

#ifndef BLINKYLIGHT_H
#define BLINKYLIGHT_H

#include <stdint.h>

/* BlinkyLight AXI-Lite base address */
#define BLINKYLIGHT_BASE ((blinkylight_t *)0x40300000)

/* BlinkyLight register structure */
typedef struct {
  const volatile u32 STATUS;
        volatile u32 GUI_CONTROL;
        volatile u32 LED_DIMMVALUE[8];
        volatile u32 SEV_SEGMENT_DISPLAY[6];
  const volatile u32 MAGIC_VALUE;
        volatile u32 IRQS;
        volatile u32 IRQ_ERRORS;
} blinkylight_t;

/* Register: BL_STATUS */
#define BL_STATUS_KEYS_Pos (0U)
#define BL_STATUS_KEYS_Len (1U)
#define BL_STATUS_KEYS_Rst (0x0U)
#define BL_STATUS_KEYS_Msk \
    (0x1U << BL_STATUS_KEYS_Pos)
#define GET_BL_STATUS_KEYS(REG) \
    (((REG) & BL_STATUS_KEYS_Msk) >> BL_STATUS_KEYS_Pos)

#define BL_STATUS_FPGA_RUNNING_Pos (1U)
#define BL_STATUS_FPGA_RUNNING_Len (1U)
#define BL_STATUS_FPGA_RUNNING_Rst (0x0U)
#define BL_STATUS_FPGA_RUNNING_Msk \
    (0x1U << BL_STATUS_FPGA_RUNNING_Pos)
#define GET_BL_STATUS_FPGA_RUNNING(REG) \
    (((REG) & BL_STATUS_FPGA_RUNNING_Msk) >> BL_STATUS_FPGA_RUNNING_Pos)

/* Register: BL_GUI_CONTROL */
#define BL_GUI_CONTROL_UPDATE_ENABLE_Pos (0U)
#define BL_GUI_CONTROL_UPDATE_ENABLE_Len (1U)
#define BL_GUI_CONTROL_UPDATE_ENABLE_Rst (0x0U)
#define BL_GUI_CONTROL_UPDATE_ENABLE_Msk \
    (0x1U << BL_GUI_CONTROL_UPDATE_ENABLE_Pos)
#define SET_BL_GUI_CONTROL_UPDATE_ENABLE(REG, VAL) \
    (((REG) & ~BL_GUI_CONTROL_UPDATE_ENABLE_Msk) | ((VAL) << BL_GUI_CONTROL_UPDATE_ENABLE_Pos))

#define BL_GUI_CONTROL_BLANK_SEVSEG_Pos (1U)
#define BL_GUI_CONTROL_BLANK_SEVSEG_Len (1U)
#define BL_GUI_CONTROL_BLANK_SEVSEG_Rst (0x0U)
#define BL_GUI_CONTROL_BLANK_SEVSEG_Msk \
    (0x1U << BL_GUI_CONTROL_BLANK_SEVSEG_Pos)
#define SET_BL_GUI_CONTROL_BLANK_SEVSEG(REG, VAL) \
    (((REG) & ~BL_GUI_CONTROL_BLANK_SEVSEG_Msk) | ((VAL) << BL_GUI_CONTROL_BLANK_SEVSEG_Pos))

/* Register: BL_LED_DIMMVALUE[N] */
#define BL_LED_DIMMVALUE_VALUE_Pos (0U)
#define BL_LED_DIMMVALUE_VALUE_Len (9U)
#define BL_LED_DIMMVALUE_VALUE_Rst (0x0U)
#define BL_LED_DIMMVALUE_VALUE_Msk \
    (0x1FFU << BL_LED_DIMMVALUE_VALUE_Pos)
#define GET_BL_LED_DIMMVALUE_VALUE(REG) \
    (((REG) & BL_LED_DIMMVALUE_VALUE_Msk) >> BL_LED_DIMMVALUE_VALUE_Pos)
#define SET_BL_LED_DIMMVALUE_VALUE(REG, VAL) \
    (((REG) & ~BL_LED_DIMMVALUE_VALUE_Msk) | ((VAL) << BL_LED_DIMMVALUE_VALUE_Pos))

/* Register: BL_SEV_SEGMENT_DISPLAY[N] */
#define BL_SEV_SEGMENT_DISPLAY_VALUE_Pos (0U)
#define BL_SEV_SEGMENT_DISPLAY_VALUE_Len (5U)
#define BL_SEV_SEGMENT_DISPLAY_VALUE_Rst (0x11U)
#define BL_SEV_SEGMENT_DISPLAY_VALUE_Msk \
    (0x1FU << BL_SEV_SEGMENT_DISPLAY_VALUE_Pos)
#define GET_BL_SEV_SEGMENT_DISPLAY_VALUE(REG) \
    (((REG) & BL_SEV_SEGMENT_DISPLAY_VALUE_Msk) >> BL_SEV_SEGMENT_DISPLAY_VALUE_Pos)
#define SET_BL_SEV_SEGMENT_DISPLAY_VALUE(REG, VAL) \
    (((REG) & ~BL_SEV_SEGMENT_DISPLAY_VALUE_Msk) | ((VAL) << BL_SEV_SEGMENT_DISPLAY_VALUE_Pos))

/* Register: BL_MAGIC_VALUE */
#define BL_MAGIC_VALUE_VALUE_Pos (0U)
#define BL_MAGIC_VALUE_VALUE_Len (32U)
#define BL_MAGIC_VALUE_VALUE_Rst (0x4711ABCDU)
#define BL_MAGIC_VALUE_VALUE_Msk \
    (0xFFFFFFFFU << BL_MAGIC_VALUE_VALUE_Pos)
#define GET_BL_MAGIC_VALUE_VALUE(REG) \
    (((REG) & BL_MAGIC_VALUE_VALUE_Msk) >> BL_MAGIC_VALUE_VALUE_Pos)

/* Register: BL_IRQS */
#define BL_IRQS_KEY_Pos (0U)
#define BL_IRQS_KEY_Len (1U)
#define BL_IRQS_KEY_Rst (0x0U)
#define BL_IRQS_KEY_Msk \
    (0x1U << BL_IRQS_KEY_Pos)
#define GET_BL_IRQS_KEY(REG) \
    (((REG) & BL_IRQS_KEY_Msk) >> BL_IRQS_KEY_Pos)
#define SET_BL_IRQS_KEY(REG, VAL) \
    (((REG) & ~BL_IRQS_KEY_Msk) | ((VAL) << BL_IRQS_KEY_Pos))

#define BL_IRQS_PPS_Pos (1U)
#define BL_IRQS_PPS_Len (1U)
#define BL_IRQS_PPS_Rst (0x0U)
#define BL_IRQS_PPS_Msk \
    (0x1U << BL_IRQS_PPS_Pos)
#define GET_BL_IRQS_PPS(REG) \
    (((REG) & BL_IRQS_PPS_Msk) >> BL_IRQS_PPS_Pos)
#define SET_BL_IRQS_PPS(REG, VAL) \
    (((REG) & ~BL_IRQS_PPS_Msk) | ((VAL) << BL_IRQS_PPS_Pos))

/* Register: BL_IRQ_ERRORS */
#define BL_IRQ_ERRORS_KEY_Pos (0U)
#define BL_IRQ_ERRORS_KEY_Len (1U)
#define BL_IRQ_ERRORS_KEY_Rst (0x0U)
#define BL_IRQ_ERRORS_KEY_Msk \
    (0x1U << BL_IRQ_ERRORS_KEY_Pos)
#define GET_BL_IRQ_ERRORS_KEY(REG) \
    (((REG) & BL_IRQ_ERRORS_KEY_Msk) >> BL_IRQ_ERRORS_KEY_Pos)
#define SET_BL_IRQ_ERRORS_KEY(REG, VAL) \
    (((REG) & ~BL_IRQ_ERRORS_KEY_Msk) | ((VAL) << BL_IRQ_ERRORS_KEY_Pos))

#define BL_IRQ_ERRORS_PPS_Pos (1U)
#define BL_IRQ_ERRORS_PPS_Len (1U)
#define BL_IRQ_ERRORS_PPS_Rst (0x0U)
#define BL_IRQ_ERRORS_PPS_Msk \
    (0x1U << BL_IRQ_ERRORS_PPS_Pos)
#define GET_BL_IRQ_ERRORS_PPS(REG) \
    (((REG) & BL_IRQ_ERRORS_PPS_Msk) >> BL_IRQ_ERRORS_PPS_Pos)
#define SET_BL_IRQ_ERRORS_PPS(REG, VAL) \
    (((REG) & ~BL_IRQ_ERRORS_PPS_Msk) | ((VAL) << BL_IRQ_ERRORS_PPS_Pos))

#endif /* BLINKYLIGHT_H */
