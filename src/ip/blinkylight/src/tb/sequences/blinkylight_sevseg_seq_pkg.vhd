-------------------------------------------------------------------------------
--! @file      blinkylight_sevseg_seq_pkg.vhd
--! @author    Michael Wurm <wurm.michael95@gmail.com>
--! @copyright 2017-2019 Michael Wurm
--! @brief     BlinkyLight Sevensegment display test sequence.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library blinkylightlib;
use blinkylightlib.blinkylight_pkg.all;

library testbenchlib;
use testbenchlib.blinkylight_uvvm_pkg.all;
use testbenchlib.blinkylight_reg_pkg.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library osvvm;
use osvvm.randompkg.all;

library bitvis_vip_axilite;
use bitvis_vip_axilite.vvc_methods_pkg.all;
use bitvis_vip_axilite.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_axilite.td_target_support_pkg.all;

library bitvis_vip_gpio;
use bitvis_vip_gpio.vvc_methods_pkg.all;
use bitvis_vip_gpio.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_gpio.td_target_support_pkg.all;

--! @brief Package declaration of blinkylight_sevseg_seq_pkg
--! @details
--! The BlinkyLight Sevensegment display test sequence.

package blinkylight_sevseg_seq_pkg is
  -----------------------------------------------------------------------------
  --! @name Types and Constants
  -----------------------------------------------------------------------------
  --! @{

  --! @}
  -----------------------------------------------------------------------------
  -- Procedures
  -----------------------------------------------------------------------------
  --! @{

  procedure set_gui_control (
    constant blank         : in    std_ulogic;
    constant update_enable : in    std_ulogic;
    constant msg_g         : in    string;
    signal axi_vvc_i       : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record);

  procedure check_sevseg_value (
    constant value_g         : in    natural;
    constant msg_g           : in    string;
    signal sevseg_gpio_vvc_i : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural);

  procedure set_sevseg_value (
    constant value_g : in    natural;
    constant addr_g  : in    unsigned(31 downto 0);
    constant msg_g   : in    string;
    signal axi_vvc_i : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record);

  procedure random_value_test (
    constant num_rand_tests_g : in    positive;
    constant addr_g           : in    unsigned(31 downto 0);
    signal axi_vvc_i          : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal sevseg_gpio_vvc_i  : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g  : in    natural);

  procedure blinkylight_sevseg_seq (
    signal start_i            : in    boolean;
    constant num_rand_tests_g : in    positive;
    constant verbose_g        : in    boolean;
    signal axi_vvc_i          : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal sevseg_gpio_vvc_i  : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g  : in    natural);

  --! @}

end package blinkylight_sevseg_seq_pkg;


package body blinkylight_sevseg_seq_pkg is

  procedure set_gui_control (
    constant blank         : in    std_ulogic;
    constant update_enable : in    std_ulogic;
    constant msg_g         : in    string;
    signal axi_vvc_i       : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record) is

    variable value : std_ulogic_vector(31 downto 0) := (others => '0');
  begin
    value := (BL_GUI_CONTROL_BLANK_SEVSEG_Pos  => blank,
              BL_GUI_CONTROL_UPDATE_ENABLE_Pos => update_enable, others => '0');
    axilite_write(axi_vvc_i, 1, BL_GUI_CONTROL_ADDR, value, msg_g);
    await_completion(axi_vvc_i, 1, axi_access_time_c + 1 ns, msg_g);
  end procedure set_gui_control;

  procedure check_sevseg_value (
    constant value_g         : in    natural;
    constant msg_g           : in    string;
    signal sevseg_gpio_vvc_i : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural) is

    variable value : std_ulogic_vector(31 downto 0) := (others => 'X');
  begin
    value(6 downto 0) := not to_sevseg(std_ulogic_vector(to_unsigned(value_g, sevseg_char_t'length)));
    gpio_check(sevseg_gpio_vvc_i, gpio_vvc_inst_g, value, msg_g, error);
    await_completion(sevseg_gpio_vvc_i, gpio_vvc_inst_g, clk_period_c, msg_g);
  end procedure check_sevseg_value;

  procedure set_sevseg_value (
    constant value_g : in    natural;
    constant addr_g  : in    unsigned(31 downto 0);
    constant msg_g   : in    string;
    signal axi_vvc_i : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record) is

    variable value : std_ulogic_vector(31 downto 0) := (others => '0');
  begin
    value(4 downto 0) := std_ulogic_vector(to_unsigned(value_g, 5));
    axilite_write(axi_vvc_i, 1, addr_g, value, msg_g);
    await_completion(axi_vvc_i, 1, axi_access_time_c + 1 ns, msg_g);
  end procedure set_sevseg_value;


  procedure random_value_test (
    constant num_rand_tests_g : in    positive;
    constant addr_g           : in    unsigned(31 downto 0);
    signal axi_vvc_i          : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal sevseg_gpio_vvc_i  : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g  : in    natural) is

    variable value_int  : natural;
    variable randomizer : RandomPType;
  begin
    enable_log_msg(axi_vvc_i, 1, ID_BFM, "", QUIET);
    enable_log_msg(ID_SEQUENCER, "", QUIET);
    log(ID_SEQUENCER, "Test randomized values.", TB_SEVSEG);
    disable_log_msg(ID_SEQUENCER, "", QUIET);

    for i in 0 to num_rand_tests_g-1 loop
      value_int := randomizer.randint(0, 17);
      set_sevseg_value(value_int, addr_g,
                       "Set character to random generated " & integer'image(value_int), axi_vvc_i);
      check_sevseg_value(value_int, "Check value on HEX", sevseg_gpio_vvc_i, gpio_vvc_inst_g);
    end loop;

    disable_log_msg(axi_vvc_i, 1, ID_BFM, "", QUIET);
  end procedure random_value_test;


  procedure blinkylight_sevseg_seq (
    signal start_i            : in    boolean;
    constant num_rand_tests_g : in    positive;
    constant verbose_g        : in    boolean;
    signal axi_vvc_i          : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal sevseg_gpio_vvc_i  : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g  : in    natural) is

    variable value_int               : natural;
    constant sevseg_idx_c            : natural := gpio_vvc_inst_g - SEVSEG_VVC_INST(0);
    constant sevseg_value_reg_addr_c : unsigned(31 downto 0) :=
      BL_SEV_SEGMENT_DISPLAY0_ADDR + sevseg_idx_c*4;
  begin
    if verbose_g = false then
      disable_log_msg(ID_LOG_HDR, "", QUIET);
      disable_log_msg(ID_SEQUENCER, "", QUIET);
      disable_log_msg(axi_vvc_i, 1, ID_LOG_MSG_CTRL, "", QUIET);
      disable_log_msg(axi_vvc_i, 1, ID_SEQUENCER, "", QUIET);
      disable_log_msg(axi_vvc_i, 1, ID_BFM, "", QUIET);
      disable_log_msg(axi_vvc_i, 1, ID_IMMEDIATE_CMD, "", QUIET);
      disable_log_msg(axi_vvc_i, 1, ID_IMMEDIATE_CMD_WAIT, "", QUIET);
      disable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_LOG_MSG_CTRL, "", QUIET);
      disable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_SEQUENCER, "", QUIET);
      disable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_BFM, "", QUIET);
      disable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_IMMEDIATE_CMD, "", QUIET);
      disable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_IMMEDIATE_CMD_WAIT, "", QUIET);
    end if;

    await_value(start_i, true, 0 ns, 10 ns, error, "Wait for SEVSEG_SEQ to enable start.", TB_SEVSEG, ID_SEQUENCER);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Check default value on HEX.", TB_SEVSEG);
    value_int := to_integer(unsigned(BL_SEV_SEGMENT_DISPLAY0_VALUE_Rst));
    check_sevseg_value(value_int, "Check default value on HEX", sevseg_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Set character to '5'.", TB_SEVSEG);
    set_sevseg_value(5, sevseg_value_reg_addr_c, "Set character to 5.", axi_vvc_i);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Should still be at default.", TB_SEVSEG);
    value_int := to_integer(unsigned(BL_SEV_SEGMENT_DISPLAY0_VALUE_Rst));
    check_sevseg_value(value_int, "Check default value on HEX", sevseg_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Blank.", TB_SEVSEG);
    set_gui_control('1', '0', "Set blank bit.", axi_vvc_i);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Should still be at default.", TB_SEVSEG);
    value_int := to_integer(unsigned(BL_SEV_SEGMENT_DISPLAY0_VALUE_Rst));
    check_sevseg_value(value_int, "Check default value on HEX", sevseg_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Release blank.", TB_SEVSEG);
    set_gui_control('0', '0', "Reset blank bit.", axi_vvc_i);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Enable GUI updates.", TB_SEVSEG);
    set_gui_control('0', '1', "Enable GUI updates.", axi_vvc_i);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Should now show '5'.", TB_SEVSEG);
    check_sevseg_value(5, "Check '5' on HEX", sevseg_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Disable GUI updates.", TB_SEVSEG);
    set_gui_control('0', '0', "Disable GUI updates.", axi_vvc_i);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Blank.", TB_SEVSEG);
    set_gui_control('1', '0', "Set blank bit.", axi_vvc_i);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Should still show '5'.", TB_SEVSEG);
    check_sevseg_value(5, "Check '5' on HEX", sevseg_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Enable GUI updates, keep blanked.", TB_SEVSEG);
    set_gui_control('1', '1', "Enable GUI updates, keep blanked.", axi_vvc_i);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Should now show blank-value.", TB_SEVSEG);
    check_sevseg_value(sevseg_char_blank_c, "Check blank-value on HEX", sevseg_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Release blank, keep updates enabled.", TB_SEVSEG);
    set_gui_control('0', '1', "Release blank, keep updates enabled.", axi_vvc_i);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Should again show '5'.", TB_SEVSEG);
    check_sevseg_value(5, "Check '5' on HEX", sevseg_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Set random values and check.", TB_SEVSEG);
    random_value_test(num_rand_tests_g, sevseg_value_reg_addr_c, axi_vvc_i, sevseg_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_SEQUENCER, "Disable GUI updates.", TB_SEVSEG);
    set_gui_control('0', '0', "Disable GUI updates.", axi_vvc_i);

    ---------------------------------------------------------------------------
    if verbose_g = false then
      enable_log_msg(ID_LOG_HDR, "", QUIET);
      enable_log_msg(ID_SEQUENCER, "", QUIET);
      enable_log_msg(axi_vvc_i, 1, ID_LOG_MSG_CTRL, "", QUIET);
      enable_log_msg(axi_vvc_i, 1, ID_SEQUENCER, "", QUIET);
      enable_log_msg(axi_vvc_i, 1, ID_BFM, "", QUIET);
      enable_log_msg(axi_vvc_i, 1, ID_IMMEDIATE_CMD, "", QUIET);
      enable_log_msg(axi_vvc_i, 1, ID_IMMEDIATE_CMD_WAIT, "", QUIET);
      enable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_LOG_MSG_CTRL, "", QUIET);
      enable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_SEQUENCER, "", QUIET);
      enable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_BFM, "", QUIET);
      enable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_IMMEDIATE_CMD, "", QUIET);
      enable_log_msg(sevseg_gpio_vvc_i, gpio_vvc_inst_g, ID_IMMEDIATE_CMD_WAIT, "", QUIET);
    end if;
  end procedure blinkylight_sevseg_seq;

end package body blinkylight_sevseg_seq_pkg;
