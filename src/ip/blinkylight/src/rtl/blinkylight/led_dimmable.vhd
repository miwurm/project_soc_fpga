-------------------------------------------------------------------------------
--! @file      led_dimmable.vhd
--! @author    Michael Wurm <wurm.michael95@gmail.com>
--! @copyright 2017-2019 Michael Wurm
--! @brief     Implementation of led_dimmable.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library blinkylightlib;
use blinkylightlib.blinkylight_pkg.all;

--! @brief Entity declaration of led_dimmable
--! @details
--! Unit can dimm LEDs.

entity led_dimmable is
  generic (
    --! System clock frequency
    clk_freq_g : natural := clk_freq_c;
    --! Number of leds to control
    num_leds_g : natural := num_of_leds_c);
  port (
    --! @name Clocks and resets
    --! @{

    --! System clock
    clk_i   : in std_ulogic;
    --! Asynchronous reset
    rst_n_i : in std_ulogic;

    --! @}
    --! @name LED output
    --! @{

    --! LED output
    led_o : out std_ulogic_vector(num_leds_g-1 downto 0);

    --! @}
    --! @name Control
    --! @{

    --! Enable updates
    enable_update_i : in std_ulogic;
    --! Dimmvalues
    dimmvalues_i    : in dimmvalues_t);

  --! @}

begin

  assert ((clk_freq_g >= 50E6) and (clk_freq_g <= 250E6))
    report "MP: Clock frequency for led_dimmable must be between 50 and 250 MHz!"
    severity error;

  assert (num_leds_g <= 10)
    report "MP: Number of LEDs must not be higher than 127 for unit led_dimmable!"
    severity error;

end entity led_dimmable;

--! Runtime configurable RTL implementation of led_dimmable
architecture rtl of led_dimmable is
  -----------------------------------------------------------------------------
  --! @name Types and Constants
  -----------------------------------------------------------------------------
  --! @{

  type state_t is (IDLE, NEXTADDR, READDATA);

  type reg_set_t is record
    state    : state_t;
    led_idx  : natural range 0 to num_leds_g;
    dimm_val : unsigned(dimmvalue_t'range);
    count    : unsigned(dimmvalue_t'range);
    leds     : std_ulogic_vector(led_o'range);
  end record reg_set_t;

  constant reg_set_init_c : reg_set_t := (
    state    => IDLE,
    led_idx  => 0,
    dimm_val => (others => '0'),
    count    => (others => '0'),
    leds     => (others => '0'));

  constant max_count_c : unsigned(dimmvalue_t'range) := (others => '1');

  --! @}
  -----------------------------------------------------------------------------
  --! @name Internal Registers
  -----------------------------------------------------------------------------
  --! @{

  signal reg_set : reg_set_t;

  --! @}
  -----------------------------------------------------------------------------
  --! @name Internal Wires
  -----------------------------------------------------------------------------
  --! @{

  signal strobe       : std_ulogic;
  signal next_reg_set : reg_set_t;

  --! @}

begin  -- architecture rtl

  -----------------------------------------------------------------------------
  -- Outputs
  -----------------------------------------------------------------------------

  led_o <= reg_set.leds;

  -----------------------------------------------------------------------------
  -- Instantiations
  -----------------------------------------------------------------------------

  strobe_gen_inst : entity blinkylightlib.strobe_gen
    generic map (
      clk_freq_g => clk_freq_g,
      period_g   => led_count_inc_period_c)
    port map (
      clk_i    => clk_i,
      rst_n_i  => rst_n_i,
      enable_i => enable_update_i,
      strobe_o => strobe);

  -----------------------------------------------------------------------------
  -- Combinatorial
  -----------------------------------------------------------------------------

  comb : process (reg_set, strobe, enable_update_i, dimmvalues_i) is
  begin
    -- Defaults
    next_reg_set <= reg_set;

    -- Dimmvalue counter
    if strobe = '1' then
      if reg_set.count = max_count_c then
        next_reg_set.count <= (others => '0');
      else
        next_reg_set.count <= reg_set.count + 1;
      end if;
    end if;

    -- State machine
    case reg_set.state is

      when IDLE =>
        if enable_update_i = '1' then
          next_reg_set.state <= NEXTADDR;
        end if;

      when NEXTADDR =>
        next_reg_set.dimm_val <= unsigned(dimmvalues_i(reg_set.led_idx));
        next_reg_set.state    <= READDATA;

      when READDATA =>
        if reg_set.dimm_val = 0 then
          next_reg_set.leds(reg_set.led_idx) <= '0';
        elsif reg_set.dimm_val = max_count_c then
          next_reg_set.leds(reg_set.led_idx) <= '1';
        elsif reg_set.count < reg_set.dimm_val then
          next_reg_set.leds(reg_set.led_idx) <= '1';
        else
          next_reg_set.leds(reg_set.led_idx) <= '0';
        end if;

        if reg_set.led_idx = num_leds_g-1 then
          next_reg_set.led_idx <= 0;
          next_reg_set.state   <= IDLE;
        else
          next_reg_set.led_idx <= reg_set.led_idx + 1;
          next_reg_set.state   <= NEXTADDR;
        end if;
    end case;
  end process comb;

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------

  regs : process(clk_i, rst_n_i)
    procedure reset is
    begin
      reg_set <= reg_set_init_c;
    end procedure reset;
  begin  -- process regs
    if rst_n_i = '0' then
      reset;
    elsif rising_edge(clk_i) then
      reg_set <= next_reg_set;
    end if;
  end process regs;

end architecture rtl;
