# Copyright (C) 2017  Intel Corporation. All rights reserved.
# Your use of Intel Corporation's design tools, logic functions
# and other software and tools, and its AMPP partner logic
# functions, and any output files from any of the foregoing
# (including device programming or simulation files), and any
# associated documentation or information are expressly subject
# to the terms and conditions of the Intel Program License
# Subscription Agreement, the Intel Quartus Prime License Agreement,
# the Intel FPGA IP License Agreement, or other applicable license
# agreement, including, without limitation, that your use is for
# the sole purpose of programming logic devices manufactured by
# Intel and sold by Intel or its authorized distributors.  Please
# refer to the applicable agreement for further details.

# Quartus Prime: Generate Tcl File for Project
# File: create_project.tcl
# Generated on: 11-18-2018
#
# This file is continuously adapted/modified during development
# by Michael Wurm <wurm.michael95@gmail.com>

# Load Quartus Prime Tcl Project package
package require ::quartus::project

set need_to_close_project 0
set make_assignments 1

set project_file [lindex $argv 0]
set project_qsf [lindex $argv 1]

# Check that the right project is open
if {[is_project_open]} {
	if {[string compare $quartus(project) "project_soc"]} {
		puts "(MWURM) Project project_soc is not open."
		set make_assignments 0
	}
} else {
	# Only open if not already open
	if {[project_exists $project_file]} {
		puts "(MWURM) Project exists. Opening."
		project_open -revision project_soc $project_file
	} else {
		puts "(MWURM) Project does not exist. Creating it."
		project_new -overwrite -revision project_soc $project_file
	}
	set need_to_close_project 1
}

# Make assignments
if {$make_assignments} {
	puts "(MWURM) Loading project assignments from: $project_qsf"

	# Load assignments
	source $project_qsf

	# Commit assignments
	export_assignments

	# Close project
	if {$need_to_close_project} {
		project_close
	}
}
