-------------------------------------------------------------------------------
--! @file      blinkylight_irq_seq_pkg.vhd
--! @author    Michael Wurm <wurm.michael95@gmail.com>
--! @copyright 2017-2019 Michael Wurm
--! @brief     BlinkyLight interrupt test sequence.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library blinkylightlib;
use blinkylightlib.blinkylight_pkg.all;

library testbenchlib;
use testbenchlib.blinkylight_uvvm_pkg.all;
use testbenchlib.blinkylight_reg_pkg.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axilite;
use bitvis_vip_axilite.vvc_methods_pkg.all;
use bitvis_vip_axilite.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_axilite.td_target_support_pkg.all;

library bitvis_vip_gpio;
use bitvis_vip_gpio.vvc_methods_pkg.all;
use bitvis_vip_gpio.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_gpio.td_target_support_pkg.all;

--! @brief Package declaration of blinkylight_irq_seq_pkg
--! @details
--! The BlinkyLight IRQ test sequence.

package blinkylight_irq_seq_pkg is
  -----------------------------------------------------------------------------
  -- Procedures
  -----------------------------------------------------------------------------
  --! @{

  procedure blinkylight_irq_seq (
    signal start_i           : in    boolean;
    signal blinky_irq_i      : in    std_ulogic;
    signal blinky_pps_i      : in    std_ulogic;
    signal axi_vvc_i         : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal keys_gpio_vvc_i   : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural);

  --! @}

end package blinkylight_irq_seq_pkg;


package body blinkylight_irq_seq_pkg is

  procedure blinkylight_irq_seq (
    signal start_i           : in    boolean;
    signal blinky_irq_i      : in    std_ulogic;
    signal blinky_pps_i      : in    std_ulogic;
    signal axi_vvc_i         : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal keys_gpio_vvc_i   : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural) is
  begin

    await_value(start_i, true, 0 ns, 10 ns, error, "Wait for IRQ_SEQ to enable start.", TB_IRQ, ID_SEQUENCER);

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Test PPS interrupt.", TB_IRQ);
    -- Reset possible pending interrupts
    axilite_write(axi_vvc_i, 1, BL_IRQS_ADDR, x"FFFFFFFF", "Clear IRQs.");
    axilite_write(axi_vvc_i, 1, BL_IRQ_ERRORS_ADDR, x"FFFFFFFF", "Clear IRQ_ERRORS.");

    -- Check if interrupt gets generated
    await_value(blinky_pps_i, '1', 0 ns, pps_period_c + 1 ns, error, "Wait for pps.", TB_IRQ, ID_SEQUENCER);
    await_value(blinky_irq_i, '1', 0 ns, 2*clk_period_c + 1 ns, error, "Wait for global interrupt to rise.", TB_IRQ, ID_SEQUENCER);

    -- Check if interrupt error gets generated
    await_value(blinky_pps_i, '1', pps_period_c - 2*clk_period_c, pps_period_c, error, "Wait for next pps.", TB_IRQ, ID_SEQUENCER);
    axilite_check(axi_vvc_i, 1, BL_IRQ_ERRORS_ADDR, x"00000002", "PPS IRQ_ERROR occured as expected.");

    -- Clear interrupts and check if they were cleared
    axilite_write(axi_vvc_i, 1, BL_IRQ_ERRORS_ADDR, x"00000002", "Clear PPS IRQ_ERROR.");
    axilite_check(axi_vvc_i, 1, BL_IRQ_ERRORS_ADDR, x"00000000", "Check if PPS IRQ_ERROR was cleared.");

    axilite_write(axi_vvc_i, 1, BL_IRQS_ADDR, x"00000002", "Clear PPS IRQ.");
    axilite_check(axi_vvc_i, 1, BL_IRQS_ADDR, x"00000000", "Check if PPS IRQ was cleared.");

    await_completion(axi_vvc_i, 1, 5 * axi_access_time_c + 6 ns, "Waiting for PPS interrupt check.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Test KEY interrupt.", TB_IRQ);

    gpio_set(keys_gpio_vvc_i, gpio_vvc_inst_g, b"0010", "Press KEY1");

    -- Check if interrupt gets generated
    await_value(blinky_irq_i, '1', 0 ns, 4*clk_period_c, error, "Wait for global interrupt to rise.", TB_IRQ, ID_SEQUENCER);
    gpio_set(keys_gpio_vvc_i, gpio_vvc_inst_g, b"0000", "Release KEY1");

    -- Press key again
    gpio_set(keys_gpio_vvc_i, gpio_vvc_inst_g, b"0010", "Press KEY1");
    wait for 4*clk_period_c;

    -- Check if interrupt error gets generated
    axilite_check(axi_vvc_i, 1, BL_IRQ_ERRORS_ADDR, x"00000001", "KEY IRQ_ERROR occured as expected.");
    gpio_set(keys_gpio_vvc_i, gpio_vvc_inst_g, b"0000", "Release KEY1");

    -- Clear interrupts and check if they were cleared
    axilite_write(axi_vvc_i, 1, BL_IRQ_ERRORS_ADDR, x"00000001", "Clear KEY IRQ_ERROR.");
    axilite_check(axi_vvc_i, 1, BL_IRQ_ERRORS_ADDR, x"00000000", "Check if KEY IRQ_ERROR was cleared.");

    axilite_write(axi_vvc_i, 1, BL_IRQS_ADDR, x"00000001", "Clear KEY IRQ.");
    axilite_check(axi_vvc_i, 1, BL_IRQS_ADDR, x"00000000", "Check if KEY IRQ was cleared.");

    await_completion(axi_vvc_i, 1, 5 * axi_access_time_c + 6 ns, "Waiting for KEY interrupt check.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Await completion.", TB_IRQ);
    await_completion(keys_gpio_vvc_i, gpio_vvc_inst_g, 200 ns, "Waiting for IRQ_SEQ to complete.");
    await_completion(axi_vvc_i, 1, 200 ns, "Waiting for IRQ_SEQ to complete.");
  end procedure blinkylight_irq_seq;

end package body blinkylight_irq_seq_pkg;
