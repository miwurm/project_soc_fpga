#-------------------------------------------------------------------------------
# file:      tb_compile.tcl
# author:    Michael Wurm <wurm.michael95@gmail.com>
# copyright: 2017-2019 Michael Wurm
# brief:     Compiles testbench.
#-------------------------------------------------------------------------------

eval transcript quietly
if [batch_mode] {
  eval echo "Detected batch mode"
  eval onerror {quit -f}
  eval onbreak {quit -f}
}

# Set compiler flags
if {[info exists param] == 0} {
  set param ""
}
if {[info exists vhdl_version] == 0} {
  set vhdl_version "-2008"
}

# Create and map work library
if {[info exists work_lib] == 0} {
  set work_lib "work"
}

eval echo [format "Mapping library: %s" $work_lib]
eval vlib $work_lib
eval vmap $work_lib $work_lib

if {[info exists src_dir] == 0} {
  set src_dir "../../src"
}

if {[info exists tb_src] == 0} {
  eval echo "(ERROR) No testbench source files defined. Please define testbench \
             source files in 'blinkylight_sim.tcl'."
  eval quit -f
}

foreach src_file $tb_src {
  set filename [format "%s/%s" $src_dir $src_file]
  eval echo [format "Compiling %s" $src_file]
  eval vcom $param $vhdl_version -work $work_lib $filename
}

if {[info exists quit_after_compile] == 0} {
  eval echo "Argument 'quit_after_compile' not set. Assuming true."
  eval quit -f
} elseif {$quit_after_compile == "true"} {
  eval quit -f
}
