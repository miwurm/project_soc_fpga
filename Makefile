#-------------------------------------------------------------------------------
# file:      Makefile
# author:    Michael Wurm <wurm.michael95@gmail.com>
# copyright: 2017-2019 Michael Wurm
# brief:     Makefile for BlinkyLight.
#-------------------------------------------------------------------------------

TOP = blinkylight
LOG = $(shell find ./build/log/*/*.wlf 2> /dev/null | tail -n 1)
GUI = 0

QUARTUS_FLAGS = --script
QSYS_FLAGS = --synthesis=VHDL --output-directory=./build/qsys --search-path=../src/**/*,$$

QUARTUS_PROJECT = ./src/board/unitHPSComputer1/project_soc
QUARTUS_QSF = ./project_soc.qsf
QSYS_PROJECT = ./src/board/unitHPSComputer1/project_soc.qsys

ips = $(shell ls ./src/ip)
specfile = ./src/ip/$1/spec/$1.yaml
templates = ./src/utils/ip/register_engine.py ./src/ip/$1/src/spec/$1.yaml ./src/utils/ip/templates/$2 $3

registers = \
	@echo "Generating registers for $1..."; \
	$(if $(shell test -f ./src/ip/$1/src/spec/$1.yaml && echo file exists), \
			$(call templates,$1,registers_axi.vhd,./src/ip/$1/src/rtl/$1_axi.vhd); \
			$(call templates,$1,registers_av_mm.vhd,./src/ip/$1/src/rtl/$1_av_mm.vhd); \
			$(call templates,$1,registers_pkg.vhd,./src/ip/$1/src/tb/$1_reg_pkg.vhd); \
			$(call templates,$1,registers.md,./src/ip/$1/registers.md); \
			$(call templates,$1,spec.vhd,./src/ip/$1/src/rtl/$1_spec_pkg.vhd); \
			$(call templates,$1,registers.txt,./build/sim/$1_register_names.txt); \
			$(call templates,$1,registers.h,./src/ip/$1/src/include/$1.h), \
		echo "Could not find specification file 'src/ip/$1/src/spec/$1.yaml'")

##--------------------------------------------------------------------
##Makefile Help for BlinkyLight.
##--------------------------------------------------------------------
##
##Supported Targets
##-----------------

all: uvvm ip test wave ##   - Default target.

build: ##                   - Synthesizes quartus project.
	@echo "Generating build output..."
	@quartus_sh $(QUARTUS_FLAGS) ./src/utils/tcl/build.tcl $(QUARTUS_PROJECT)
	@echo Done.

project: ##                 - Creates/Opens quartus project. Requires qsys project to exist.
	@echo "Creating/Opening Quartus project..."
	@quartus_sh $(QUARTUS_FLAGS) ./src/utils/tcl/create_project.tcl $(QUARTUS_PROJECT) $(QUARTUS_QSF)
ifeq ($(GUI),1)
	@quartus $(QUARTUS_PROJECT).qpf
endif
	@echo Done.

qsys: ##                    - Creates the Qsys Project
	@echo "Creating Qsys project..."
	@qsys-generate $(QSYS_PROJECT) $(QSYS_FLAGS)
	@echo Done.

firmware: ##                - Opens Intel FPGA Monitor Program
	@echo "Opening Intel FPGA Monitor Program."
	@intel-fpga-monitor-program

dependencies: ##            - Install dependencies.
	@echo "Installing required dependencies."
	@sudo pip2 install pyyaml jinja2 numpy

uvvm: ##                    - Generate UVVM simulation files.
	@echo "Generating UVVM simulation files..."
ifneq ($(MODELSIM_VERSION),10.6b)
	@echo "(MWURM) ERROR: MODELSIM_VERSION = $(MODELSIM_VERSION), UVVM requires version 10.6b"
	@exit 1
endif
	@mkdir -p ./build/sim
	@mkdir -p ./build/log
	@cd ./build/sim && vsim -do ../../src/utils/tcl/uvvm_compile.tcl -c 2>&1 | tee ../log/uvvm_compile.log
	@! grep -q "\*\* Error" ./build/log/uvvm_compile.log
	@! grep -q "\*\* Fatal" ./build/log/uvvm_compile.log

ip: registers ##            - Generate IP simulation files.
	@echo "Generating IP simulation files..."
	@mkdir -p ./build/sim
	@mkdir -p ./build/log
	@cd ./build/sim && vsim -do ../../src/utils/tcl/ip_compile.tcl -c 2>&1 | tee ../log/ip_compile.log
	@! grep -q "\*\* Error" ./build/log/ip_compile.log
	@! grep -q "\*\* Fatal" ./build/log/ip_compile.log

registers: ##               - Generate IP registers from templates.
	@mkdir -p ./build/sim
	$(foreach ip,$(ips), $(call registers,$(ip)))

##
test: registers ##          - Run verification.
	@echo "Running testbench..."
	@mkdir -p ./build/sim
	@mkdir -p ./build/log
	@cd build/sim && vsim -do ../../src/utils/tcl/sim.tcl -c 2>&1 -Gtestbench_top=$(TOP) | tee ../log/test.log
	@! grep -q "\*\* Error" ./build/log/test.log
	@! grep -q "\*\* Fatal" ./build/log/test.log

wave: ##                    - Open simulation log.
	@echo "Opening waveform..."
	@cd build/sim && vsim -do ../../src/utils/tcl/wave.tcl -Gopen_log=$(LOG) -Gtestbench_top=$(TOP) 2> /dev/null

docs: ##                    - Build documentation.
	@echo "Generating documentation..."
	@mkdir -p ./build/doc
	@cd doc/ && doxygen Doxyfile

linting: ##                 - Linting for RTL code.
	@./src/utils/ip/lint.sh

clean: ##                   - Clean outputs.
	@echo "Cleaning..."
	git clean -x -d -f

help: ##                    - Show this help message.
	@grep -h "##" Makefile | grep -v "\"##\"" | sed -e 's/##//g'

.PHONY: all build project firmware dependencies uvvm ip registers test wave docs clean help

##
##--------------------------------------------------------------------
##*** NOTE: Makefile requires following setup:
##
##      - Modelsim (vsim) can be found in PATH.
##      - Intel Quartus (quartus) can be found in PATH.
##      - Intel Quartus Qsys (qsys-generate) can be found in PATH.
##
##--------------------------------------------------------------------
