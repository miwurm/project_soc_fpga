#-------------------------------------------------------------------------------
# file:      wave_gen.tcl
# author:    Michael Wurm <wurm.michael95@gmail.com>
# copyright: 2017-2019 Michael Wurm
# brief:     Generates waveform of all signals.
#-------------------------------------------------------------------------------

set sig_list {}
foreach wave_pattern $wave_patterns {
  set find_param ""
  if {[lindex $wave_pattern 1] == 1} {
    set find_param "-recursive"
  }
  set int_list [eval find signals -internal $find_param [lindex $wave_pattern 0]]
  set in_list [eval find signals -in $find_param [lindex $wave_pattern 0]]
  set out_list [eval find signals -out $find_param [lindex $wave_pattern 0]]
  set inout_list [eval find signals -inout $find_param [lindex $wave_pattern 0]]
  set blk_list [eval find blocks -nodu $find_param [lindex $wave_pattern 0]]
  foreach int_list_item $int_list {
    if {[string match "*#*" $int_list_item] == 0} {
      lappend sig_list [list $int_list_item 0]
    }
  }
  foreach in_list_item $in_list {
    lappend sig_list [list $in_list_item 1]
  }
  foreach out_list_item $out_list {
    lappend sig_list [list $out_list_item 2]
  }
  foreach inout_list_item $inout_list {
    lappend sig_list [list $inout_list_item 3]
  }
  foreach blk_list_item $blk_list {
    if {[string length $blk_list_item] > 0 && \
        [string match "*\(*\)*" $blk_list_item] == 0 && \
        [string match "*\\\[*\\\]*" $blk_list_item] == 0 && \
        [string match "*#*" $blk_list_item] == 0} {
      lappend sig_list [list $blk_list_item 4]
    }
  }
}
set sig_list [lsort -unique -dictionary -index 0 $sig_list]
foreach sig $sig_list {
  set name [string trim [lindex $sig 0]]
  set type [lindex $sig 1]
  set ignore 0
  foreach ignore_pattern $wave_ignores {
    if {[string match [string trim $ignore_pattern] $name] == 1} {
      set ignore 1
    }
  }
  if {$ignore == 0} {
    set path [split $name "/"]
    set wave_param ""
    for {set x 1} {$x < [expr [llength $path] - 1]} {incr x} {
      append wave_param [format "%s -group %s " $wave_expand_param [lindex $path $x]]
    }
    if {$type == 0} {
      append wave_param [format "%s -group Internal" $wave_expand_param]
    } elseif {$type == 1} {
      append wave_param [format "%s -group Ports %s -group In" $wave_expand_param $wave_expand_param]
    } elseif {$type == 2} {
      append wave_param [format "%s -group Ports %s -group Out" $wave_expand_param $wave_expand_param]
    } elseif {$type == 3} {
      append wave_param [format "%s -group Ports %s -group InOut" $wave_expand_param $wave_expand_param]
    } elseif {$type == 4} {
      append wave_param [format "%s -group Assertions" $wave_expand_param]
    }
    set label [lindex $path [expr [llength $path] - 1]]
    append wave_param [format " -label %s" $label]
    set wave_cmd [format "add wave -radix %s %s %s" $wave_radix $wave_param $name]
    set wave_cmd [regsub -all {\[} $wave_cmd {\\\\\[}]
    set wave_cmd [regsub -all {\]} $wave_cmd {\\\\\]}]
    if {[catch {eval $wave_cmd} errmsg]} {
      eval echo [format "Wave ERROR: %s" $errmsg]
    }
  }
}
