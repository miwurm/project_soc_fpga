--------------------------------------------------------------------------------
-- Title       : Modem Channel Selection Controller
-- Project     : FPGA Based Digital Signal Processing
--               FH OÖ Hagenberg/HSD, SCD5
--------------------------------------------------------------------------------
-- RevCtrl     : $Id: ModemChannelSelector.vhd 755 2017-12-10 22:43:13Z mroland $
-- Authors     : Michael Roland, Hagenberg/Austria, Copyright (c) 2017
--------------------------------------------------------------------------------
-- Description : Avalon memory-mapped slave that allows selection of a receiver/
--             : transmitter channel for the FSK modem
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ModemChannelSelector is
  generic (
    CHANNEL_WIDTH   : natural := 4;
    CHANNEL_DEFAULT : natural := 0
  );
  port (
    clk            : in  std_logic                     := '0';             --   clock.clk
    reset_n        : in  std_logic                     := '0';             --   reset.reset_n
    channel        : out std_logic_vector(CHANNEL_WIDTH-1 downto 0);       -- conduit.output
    s1_address     : in  std_logic_vector(0 downto 0)  := (others => '0'); --      s1.address
    s1_read        : in  std_logic                     := '0';             --        .read
    s1_readdata    : out std_logic_vector(31 downto 0);                    --        .readdata
    s1_write       : in  std_logic                     := '0';             --        .write
    s1_writedata   : in  std_logic_vector(31 downto 0) := (others => '0')  --        .writedata
  );
end entity ModemChannelSelector;

architecture rtl of ModemChannelSelector is

  subtype aAddress is natural range 0 to (2**s1_address'length - 1);
  constant cMagicValue : std_logic_vector(s1_readdata'range) := X"5CD01234";

  signal ChannelConfigReg : std_ulogic_vector(s1_readdata'range) :=
          std_ulogic_vector(to_unsigned(CHANNEL_DEFAULT,s1_readdata'length));

begin

  AvalonMMInterface : process(clk)
    variable vAddress : aAddress;
  begin
    if rising_edge(clk) then
      if reset_n = '0' then
        -- synchronous reset
        s1_readdata   <= (others => '0');
        ChannelConfigReg <=
          std_ulogic_vector(to_unsigned(CHANNEL_DEFAULT,ChannelConfigReg'length));

      else
        -- transform address bits to integer value
        vAddress := to_integer(unsigned(s1_address));

        if s1_read = '1' then
          case vAddress is
            when 0 => s1_readdata <= std_logic_vector(ChannelConfigReg);
            when 1 => s1_readdata <= cMagicValue;
            when others => report "ModemChannelSelector: address unreachable" severity failure;
          end case;
        end if;

        if s1_write = '1' then
          case vAddress is
            when 0 => ChannelConfigReg <= std_ulogic_vector(s1_writedata);
            when others => report "ModemChannelSelector: s1_write address unreachable" severity failure;
          end case;
        end if;

      end if;
    end if;
  end process;

  channel <= std_logic_vector(ChannelConfigReg(channel'range));

end architecture rtl; -- of ModemChannelSelector
