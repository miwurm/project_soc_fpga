-------------------------------------------------------------------------------
--! @file      HPSComputer1.vhd
--! @author    Michael Wurm <wurm.michael95@gmail.com>
--! @copyright 2017-2019 Michael Wurm
--! @brief     HPS Instantiation.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @brief Entity declaration of HPSComputer1
--! @details
--! Instantiation of the Project SoC IP.
entity HPSComputer1 is
  port (
    --! @name System clock
    --! @{

    clk_i : in std_ulogic;

    --! @}
    --! @name User interface hardware
    --! @{

    key_i    : in std_ulogic_vector(3 downto 0);
    switch_i : in std_ulogic_vector(9 downto 0);

    ledr_o : out std_ulogic_vector(7 downto 0);
    hex0_o : out std_ulogic_vector(6 downto 0);
    hex1_o : out std_ulogic_vector(6 downto 0);
    hex2_o : out std_ulogic_vector(6 downto 0);
    hex3_o : out std_ulogic_vector(6 downto 0);
    hex4_o : out std_ulogic_vector(6 downto 0);
    hex5_o : out std_ulogic_vector(6 downto 0);

    --! @}
    --! @name HPS Interface
    --! @{

    -- DDR3 SDRAM
    HPS_DDR3_ADDR    : out   std_logic_vector(14 downto 0);
    HPS_DDR3_BA      : out   std_logic_vector(2 downto 0);
    HPS_DDR3_CK_P    : out   std_logic;
    HPS_DDR3_CK_N    : out   std_logic;
    HPS_DDR3_CKE     : out   std_logic;
    HPS_DDR3_CS_N    : out   std_logic;
    HPS_DDR3_RAS_N   : out   std_logic;
    HPS_DDR3_CAS_N   : out   std_logic;
    HPS_DDR3_WE_N    : out   std_logic;
    HPS_DDR3_RESET_N : out   std_logic;
    HPS_DDR3_DQ      : inout std_logic_vector(31 downto 0);
    HPS_DDR3_DQS_P   : inout std_logic_vector(3 downto 0);
    HPS_DDR3_DQS_N   : inout std_logic_vector(3 downto 0);
    HPS_DDR3_ODT     : out   std_logic;
    HPS_DDR3_DM      : out   std_logic_vector(3 downto 0);
    HPS_DDR3_RZQ     : in    std_logic;

    -- ETHERNET
    HPS_ENET_GTX_CLK : out   std_logic;
    HPS_ENET_MDC     : out   std_logic;
    HPS_ENET_MDIO    : inout std_logic;
    HPS_ENET_RX_CLK  : in    std_logic;
    HPS_ENET_RX_DATA : in    std_logic_vector(3 downto 0);
    HPS_ENET_RX_DV   : in    std_logic;
    HPS_ENET_TX_DATA : out   std_logic_vector(3 downto 0);
    HPS_ENET_TX_EN   : out   std_logic;
    HPS_ENET_INT_N   : inout std_logic;

    -- QSPI FLASH
    HPS_FLASH_DATA : inout std_logic_vector(3 downto 0);
    HPS_FLASH_DCLK : out   std_logic;
    HPS_FLASH_NCSO : out   std_logic;

    -- I2C
    HPS_I2C_CONTROL : inout std_logic;
    HPS_I2C1_SCLK   : inout std_logic;
    HPS_I2C1_SDAT   : inout std_logic;
    HPS_I2C2_SCLK   : inout std_logic;
    HPS_I2C2_SDAT   : inout std_logic;

    -- SD CARD
    HPS_SD_CMD  : inout std_logic;
    HPS_SD_CLK  : out   std_logic;
    HPS_SD_DATA : inout std_logic_vector(3 downto 0);

    -- USB
    HPS_USB_CLKOUT : in    std_logic;
    HPS_USB_DATA   : inout std_logic_vector(7 downto 0);
    HPS_USB_DIR    : in    std_logic;
    HPS_USB_NXT    : in    std_logic;
    HPS_USB_STP    : out   std_logic;
    HPS_CONV_USB_N : inout std_logic;

    -- SPI
    HPS_SPIM_CLK  : out std_logic;
    HPS_SPIM_MISO : in  std_logic;
    HPS_SPIM_MOSI : out std_logic;
    HPS_SPIM_SS   : out std_logic;

    -- UART
    HPS_UART_TX : inout std_logic;
    HPS_UART_RX : inout std_logic;

    -- GPIO
    HPS_KEY         : inout std_logic;
    HPS_LED         : inout std_logic;
    HPS_LTC_GPIO    : inout std_logic;
    HPS_GSENSOR_INT : inout std_logic);

    --! @}

end entity HPSComputer1;


architecture Inst of HPSComputer1 is

  -----------------------------------------------------------------------------
  --! @name Components
  -----------------------------------------------------------------------------
  --! @{

  component project_soc is
    port (
      blinky_hex0_output               : out   std_logic_vector(6 downto 0);                     --      blinky_hex0.output
      blinky_hex1_output               : out   std_logic_vector(6 downto 0);                     --      blinky_hex1.output
      blinky_hex2_output               : out   std_logic_vector(6 downto 0);                     --      blinky_hex2.output
      blinky_hex3_output               : out   std_logic_vector(6 downto 0);                     --      blinky_hex3.output
      blinky_hex4_output               : out   std_logic_vector(6 downto 0);                     --      blinky_hex4.output
      blinky_hex5_output               : out   std_logic_vector(6 downto 0);                     --      blinky_hex5.output
      blinky_irq_output                : out   std_logic;                                        --       blinky_irq.output
      blinky_key_input                 : in    std_logic_vector(2 downto 0)  := (others => '0'); --       blinky_key.input
      blinky_led_output                : out   std_logic_vector(7 downto 0);                     --       blinky_led.output
      blinky_pps_output                : out   std_logic;                                        --       blinky_pps.output
      blinky_running_output            : out   std_logic;                                        --   blinky_running.output
      blinky_switch_input              : in    std_logic_vector(9 downto 0)  := (others => '0'); --    blinky_switch.input
      h2f_loan_io_in                   : out   std_logic_vector(66 downto 0);                    --      h2f_loan_io.in
      h2f_loan_io_out                  : in    std_logic_vector(66 downto 0) := (others => '0'); --                 .out
      h2f_loan_io_oe                   : in    std_logic_vector(66 downto 0) := (others => '0'); --                 .oe
      hps_io_hps_io_emac1_inst_TX_CLK  : out   std_logic;                                        --           hps_io.hps_io_emac1_inst_TX_CLK
      hps_io_hps_io_emac1_inst_TXD0    : out   std_logic;                                        --                 .hps_io_emac1_inst_TXD0
      hps_io_hps_io_emac1_inst_TXD1    : out   std_logic;                                        --                 .hps_io_emac1_inst_TXD1
      hps_io_hps_io_emac1_inst_TXD2    : out   std_logic;                                        --                 .hps_io_emac1_inst_TXD2
      hps_io_hps_io_emac1_inst_TXD3    : out   std_logic;                                        --                 .hps_io_emac1_inst_TXD3
      hps_io_hps_io_emac1_inst_RXD0    : in    std_logic                     := '0';             --                 .hps_io_emac1_inst_RXD0
      hps_io_hps_io_emac1_inst_MDIO    : inout std_logic                     := '0';             --                 .hps_io_emac1_inst_MDIO
      hps_io_hps_io_emac1_inst_MDC     : out   std_logic;                                        --                 .hps_io_emac1_inst_MDC
      hps_io_hps_io_emac1_inst_RX_CTL  : in    std_logic                     := '0';             --                 .hps_io_emac1_inst_RX_CTL
      hps_io_hps_io_emac1_inst_TX_CTL  : out   std_logic;                                        --                 .hps_io_emac1_inst_TX_CTL
      hps_io_hps_io_emac1_inst_RX_CLK  : in    std_logic                     := '0';             --                 .hps_io_emac1_inst_RX_CLK
      hps_io_hps_io_emac1_inst_RXD1    : in    std_logic                     := '0';             --                 .hps_io_emac1_inst_RXD1
      hps_io_hps_io_emac1_inst_RXD2    : in    std_logic                     := '0';             --                 .hps_io_emac1_inst_RXD2
      hps_io_hps_io_emac1_inst_RXD3    : in    std_logic                     := '0';             --                 .hps_io_emac1_inst_RXD3
      hps_io_hps_io_qspi_inst_IO0      : inout std_logic                     := '0';             --                 .hps_io_qspi_inst_IO0
      hps_io_hps_io_qspi_inst_IO1      : inout std_logic                     := '0';             --                 .hps_io_qspi_inst_IO1
      hps_io_hps_io_qspi_inst_IO2      : inout std_logic                     := '0';             --                 .hps_io_qspi_inst_IO2
      hps_io_hps_io_qspi_inst_IO3      : inout std_logic                     := '0';             --                 .hps_io_qspi_inst_IO3
      hps_io_hps_io_qspi_inst_SS0      : out   std_logic;                                        --                 .hps_io_qspi_inst_SS0
      hps_io_hps_io_qspi_inst_CLK      : out   std_logic;                                        --                 .hps_io_qspi_inst_CLK
      hps_io_hps_io_sdio_inst_CMD      : inout std_logic                     := '0';             --                 .hps_io_sdio_inst_CMD
      hps_io_hps_io_sdio_inst_D0       : inout std_logic                     := '0';             --                 .hps_io_sdio_inst_D0
      hps_io_hps_io_sdio_inst_D1       : inout std_logic                     := '0';             --                 .hps_io_sdio_inst_D1
      hps_io_hps_io_sdio_inst_CLK      : out   std_logic;                                        --                 .hps_io_sdio_inst_CLK
      hps_io_hps_io_sdio_inst_D2       : inout std_logic                     := '0';             --                 .hps_io_sdio_inst_D2
      hps_io_hps_io_sdio_inst_D3       : inout std_logic                     := '0';             --                 .hps_io_sdio_inst_D3
      hps_io_hps_io_usb1_inst_D0       : inout std_logic                     := '0';             --                 .hps_io_usb1_inst_D0
      hps_io_hps_io_usb1_inst_D1       : inout std_logic                     := '0';             --                 .hps_io_usb1_inst_D1
      hps_io_hps_io_usb1_inst_D2       : inout std_logic                     := '0';             --                 .hps_io_usb1_inst_D2
      hps_io_hps_io_usb1_inst_D3       : inout std_logic                     := '0';             --                 .hps_io_usb1_inst_D3
      hps_io_hps_io_usb1_inst_D4       : inout std_logic                     := '0';             --                 .hps_io_usb1_inst_D4
      hps_io_hps_io_usb1_inst_D5       : inout std_logic                     := '0';             --                 .hps_io_usb1_inst_D5
      hps_io_hps_io_usb1_inst_D6       : inout std_logic                     := '0';             --                 .hps_io_usb1_inst_D6
      hps_io_hps_io_usb1_inst_D7       : inout std_logic                     := '0';             --                 .hps_io_usb1_inst_D7
      hps_io_hps_io_usb1_inst_CLK      : in    std_logic                     := '0';             --                 .hps_io_usb1_inst_CLK
      hps_io_hps_io_usb1_inst_STP      : out   std_logic;                                        --                 .hps_io_usb1_inst_STP
      hps_io_hps_io_usb1_inst_DIR      : in    std_logic                     := '0';             --                 .hps_io_usb1_inst_DIR
      hps_io_hps_io_usb1_inst_NXT      : in    std_logic                     := '0';             --                 .hps_io_usb1_inst_NXT
      hps_io_hps_io_spim1_inst_CLK     : out   std_logic;                                        --                 .hps_io_spim1_inst_CLK
      hps_io_hps_io_spim1_inst_MOSI    : out   std_logic;                                        --                 .hps_io_spim1_inst_MOSI
      hps_io_hps_io_spim1_inst_MISO    : in    std_logic                     := '0';             --                 .hps_io_spim1_inst_MISO
      hps_io_hps_io_spim1_inst_SS0     : out   std_logic;                                        --                 .hps_io_spim1_inst_SS0
      hps_io_hps_io_uart0_inst_RX      : in    std_logic                     := '0';             --                 .hps_io_uart0_inst_RX
      hps_io_hps_io_uart0_inst_TX      : out   std_logic;                                        --                 .hps_io_uart0_inst_TX
      hps_io_hps_io_i2c0_inst_SDA      : inout std_logic                     := '0';             --                 .hps_io_i2c0_inst_SDA
      hps_io_hps_io_i2c0_inst_SCL      : inout std_logic                     := '0';             --                 .hps_io_i2c0_inst_SCL
      hps_io_hps_io_i2c1_inst_SDA      : inout std_logic                     := '0';             --                 .hps_io_i2c1_inst_SDA
      hps_io_hps_io_i2c1_inst_SCL      : inout std_logic                     := '0';             --                 .hps_io_i2c1_inst_SCL
      hps_io_hps_io_gpio_inst_GPIO09   : inout std_logic                     := '0';             --                 .hps_io_gpio_inst_GPIO09
      hps_io_hps_io_gpio_inst_GPIO35   : inout std_logic                     := '0';             --                 .hps_io_gpio_inst_GPIO35
      hps_io_hps_io_gpio_inst_GPIO40   : inout std_logic                     := '0';             --                 .hps_io_gpio_inst_GPIO40
      hps_io_hps_io_gpio_inst_GPIO48   : inout std_logic                     := '0';             --                 .hps_io_gpio_inst_GPIO48
      hps_io_hps_io_gpio_inst_GPIO61   : inout std_logic                     := '0';             --                 .hps_io_gpio_inst_GPIO61
      hps_io_hps_io_gpio_inst_LOANIO53 : inout std_logic                     := '0';             --                 .hps_io_gpio_inst_LOANIO53
      hps_io_hps_io_gpio_inst_LOANIO54 : inout std_logic                     := '0';             --                 .hps_io_gpio_inst_LOANIO54
      hps_uart1_cts                    : in    std_logic                     := '0';             --        hps_uart1.cts
      hps_uart1_dsr                    : in    std_logic                     := '0';             --                 .dsr
      hps_uart1_dcd                    : in    std_logic                     := '0';             --                 .dcd
      hps_uart1_ri                     : in    std_logic                     := '0';             --                 .ri
      hps_uart1_dtr                    : out   std_logic;                                        --                 .dtr
      hps_uart1_rts                    : out   std_logic;                                        --                 .rts
      hps_uart1_out1_n                 : out   std_logic;                                        --                 .out1_n
      hps_uart1_out2_n                 : out   std_logic;                                        --                 .out2_n
      hps_uart1_rxd                    : in    std_logic                     := '0';             --                 .rxd
      hps_uart1_txd                    : out   std_logic;                                        --                 .txd
      memory_mem_a                     : out   std_logic_vector(14 downto 0);                    --           memory.mem_a
      memory_mem_ba                    : out   std_logic_vector(2 downto 0);                     --                 .mem_ba
      memory_mem_ck                    : out   std_logic;                                        --                 .mem_ck
      memory_mem_ck_n                  : out   std_logic;                                        --                 .mem_ck_n
      memory_mem_cke                   : out   std_logic;                                        --                 .mem_cke
      memory_mem_cs_n                  : out   std_logic;                                        --                 .mem_cs_n
      memory_mem_ras_n                 : out   std_logic;                                        --                 .mem_ras_n
      memory_mem_cas_n                 : out   std_logic;                                        --                 .mem_cas_n
      memory_mem_we_n                  : out   std_logic;                                        --                 .mem_we_n
      memory_mem_reset_n               : out   std_logic;                                        --                 .mem_reset_n
      memory_mem_dq                    : inout std_logic_vector(31 downto 0) := (others => '0'); --                 .mem_dq
      memory_mem_dqs                   : inout std_logic_vector(3 downto 0)  := (others => '0'); --                 .mem_dqs
      memory_mem_dqs_n                 : inout std_logic_vector(3 downto 0)  := (others => '0'); --                 .mem_dqs_n
      memory_mem_odt                   : out   std_logic;                                        --                 .mem_odt
      memory_mem_dm                    : out   std_logic_vector(3 downto 0);                     --                 .mem_dm
      memory_oct_rzqin                 : in    std_logic                     := '0';             --                 .oct_rzqin
      --modem_rx_channel_output          : out   std_logic_vector(3 downto 0);                     -- modem_rx_channel.output
      --modem_tx_channel_output          : out   std_logic_vector(3 downto 0);                     -- modem_tx_channel.output
      --seven_seg_0_output               : out   std_logic_vector(6 downto 0);                     --      seven_seg_0.output
      --seven_seg_1_output               : out   std_logic_vector(6 downto 0);                     --      seven_seg_1.output
      system_clk_clk                   : in    std_logic                     := '0';             --       system_clk.clk
      system_reset_reset               : in    std_logic                     := '0'              --     system_reset.reset
    );
  end component project_soc;

  --@}
  -----------------------------------------------------------------------------
  --! @name Internal Wires
  -----------------------------------------------------------------------------
  --! @{

  signal rst_n       : std_ulogic;

  signal pps         : std_ulogic;
  signal running     : std_ulogic;
  signal blinky_keys : std_ulogic_vector(2 downto 0);
  signal blinky_leds : std_ulogic_vector(7 downto 0);

  signal h2f_loan_io_in  : std_ulogic_vector(66 downto 0);
  signal h2f_loan_io_out : std_ulogic_vector(66 downto 0);
  signal h2f_loan_io_oe  : std_ulogic_vector(66 downto 0);

  constant LOAN_IO_LED : natural := 53;
  constant LOAN_IO_KEY : natural := 54;

  signal hps_key_input         : std_ulogic;
  signal hps_led_output        : std_ulogic;
  signal hps_fpga_uart_forward : std_ulogic;

  --@}

begin  -- architecture Inst

  -----------------------------------------------------------------------------
  -- Outputs
  -----------------------------------------------------------------------------

  ledr_o(4 downto 0) <= blinky_leds(4 downto 0);
  ledr_o(5)          <= hps_led_output;
  ledr_o(6)          <= pps;
  ledr_o(7)          <= running;

  -- Use HPS USER LED as output
  h2f_loan_io_oe(LOAN_IO_LED)  <= '1'; -- configure as output
  h2f_loan_io_out(LOAN_IO_LED) <= hps_led_output;

  -----------------------------------------------------------------------------
  -- Signal Assignments
  -----------------------------------------------------------------------------

  rst_n       <= key_i(0);
  blinky_keys <= key_i(3 downto 1);

  -- Use HPS USER BUTTON as input
  h2f_loan_io_oe(LOAN_IO_KEY) <= '0'; -- configure as input
  hps_key_input               <= h2f_loan_io_in(LOAN_IO_KEY);
  hps_led_output              <= not(hps_key_input);

  -----------------------------------------------------------------------------
  -- Instantiations
  -----------------------------------------------------------------------------

  project_soc_inst : component project_soc
    port map (
      -- Clocks and resets
      system_clk_clk     => clk_i,
      system_reset_reset => rst_n,

      -- BlinkyLight visual interface
      std_ulogic_vector(blinky_hex0_output) => hex0_o,
      std_ulogic_vector(blinky_hex1_output) => hex1_o,
      std_ulogic_vector(blinky_hex2_output) => hex2_o,
      std_ulogic_vector(blinky_hex3_output) => hex3_o,
      std_ulogic_vector(blinky_hex4_output) => hex4_o,
      std_ulogic_vector(blinky_hex5_output) => hex5_o,
      std_ulogic_vector(blinky_led_output)  => blinky_leds,

      -- BlinkyLight physical user input
      blinky_key_input    => std_logic_vector(blinky_keys),
      blinky_switch_input => std_logic_vector(switch_i),

      -- BlinkyLight status output
      blinky_irq_output                 => open,
      std_ulogic(blinky_pps_output)     => pps,
      std_ulogic(blinky_running_output) => running,

      -- Other IP interfaces
      --modem_rx_channel_output => open,
      --modem_tx_channel_output => open,
      --seven_seg_0_output      => open,
      --seven_seg_1_output      => open,

      -- HPS LOAN I/O CONTROL
      std_logic_vector(h2f_loan_io_in) => h2f_loan_io_in,
      h2f_loan_io_out                  => std_logic_vector(h2f_loan_io_out),
      h2f_loan_io_oe                   => std_logic_vector(h2f_loan_io_oe),

      -- HPS UART1
      hps_uart1_cts    => '1',
      hps_uart1_dsr    => '1',
      hps_uart1_dcd    => '0',
      hps_uart1_ri     => '0',
      hps_uart1_dtr    => open,
      hps_uart1_rts    => open,
      hps_uart1_out1_n => open,
      hps_uart1_out2_n => open,
      hps_uart1_rxd    => hps_fpga_uart_forward,
      hps_uart1_txd    => hps_fpga_uart_forward,

      -- DDR3 SDRAM
      memory_mem_a       => HPS_DDR3_ADDR,
      memory_mem_ba      => HPS_DDR3_BA,
      memory_mem_ck      => HPS_DDR3_CK_P,
      memory_mem_ck_n    => HPS_DDR3_CK_N,
      memory_mem_cke     => HPS_DDR3_CKE,
      memory_mem_cs_n    => HPS_DDR3_CS_N,
      memory_mem_ras_n   => HPS_DDR3_RAS_N,
      memory_mem_cas_n   => HPS_DDR3_CAS_N,
      memory_mem_we_n    => HPS_DDR3_WE_N,
      memory_mem_reset_n => HPS_DDR3_RESET_N,
      memory_mem_dq      => HPS_DDR3_DQ,
      memory_mem_dqs     => HPS_DDR3_DQS_P,
      memory_mem_dqs_n   => HPS_DDR3_DQS_N,
      memory_mem_odt     => HPS_DDR3_ODT,
      memory_mem_dm      => HPS_DDR3_DM,
      memory_oct_rzqin   => HPS_DDR3_RZQ,

      -- ETHERNET
      hps_io_hps_io_emac1_inst_TX_CLK => HPS_ENET_GTX_CLK,
      hps_io_hps_io_emac1_inst_TXD0   => HPS_ENET_TX_DATA(0),
      hps_io_hps_io_emac1_inst_TXD1   => HPS_ENET_TX_DATA(1),
      hps_io_hps_io_emac1_inst_TXD2   => HPS_ENET_TX_DATA(2),
      hps_io_hps_io_emac1_inst_TXD3   => HPS_ENET_TX_DATA(3),
      hps_io_hps_io_emac1_inst_RXD0   => HPS_ENET_RX_DATA(0),
      hps_io_hps_io_emac1_inst_MDIO   => HPS_ENET_MDIO,
      hps_io_hps_io_emac1_inst_MDC    => HPS_ENET_MDC,
      hps_io_hps_io_emac1_inst_RX_CTL => HPS_ENET_RX_DV,
      hps_io_hps_io_emac1_inst_TX_CTL => HPS_ENET_TX_EN,
      hps_io_hps_io_emac1_inst_RX_CLK => HPS_ENET_RX_CLK,
      hps_io_hps_io_emac1_inst_RXD1   => HPS_ENET_RX_DATA(1),
      hps_io_hps_io_emac1_inst_RXD2   => HPS_ENET_RX_DATA(2),
      hps_io_hps_io_emac1_inst_RXD3   => HPS_ENET_RX_DATA(3),
      hps_io_hps_io_gpio_inst_GPIO35  => HPS_ENET_INT_N,

      -- QSPI FLASH
      hps_io_hps_io_qspi_inst_IO0 => HPS_FLASH_DATA(0),
      hps_io_hps_io_qspi_inst_IO1 => HPS_FLASH_DATA(1),
      hps_io_hps_io_qspi_inst_IO2 => HPS_FLASH_DATA(2),
      hps_io_hps_io_qspi_inst_IO3 => HPS_FLASH_DATA(3),
      hps_io_hps_io_qspi_inst_SS0 => HPS_FLASH_NCSO,
      hps_io_hps_io_qspi_inst_CLK => HPS_FLASH_DCLK,

      -- I2C
      hps_io_hps_io_gpio_inst_GPIO48 => HPS_I2C_CONTROL,
      hps_io_hps_io_i2c0_inst_SCL    => HPS_I2C1_SCLK,
      hps_io_hps_io_i2c0_inst_SDA    => HPS_I2C1_SDAT,
      hps_io_hps_io_i2c1_inst_SCL    => HPS_I2C2_SCLK,
      hps_io_hps_io_i2c1_inst_SDA    => HPS_I2C2_SDAT,

      -- SD CARD
      hps_io_hps_io_sdio_inst_CMD => HPS_SD_CMD,
      hps_io_hps_io_sdio_inst_D0  => HPS_SD_DATA(0),
      hps_io_hps_io_sdio_inst_D1  => HPS_SD_DATA(1),
      hps_io_hps_io_sdio_inst_CLK => HPS_SD_CLK,
      hps_io_hps_io_sdio_inst_D2  => HPS_SD_DATA(2),
      hps_io_hps_io_sdio_inst_D3  => HPS_SD_DATA(3),

      -- USB
      hps_io_hps_io_usb1_inst_D0     => HPS_USB_DATA(0),
      hps_io_hps_io_usb1_inst_D1     => HPS_USB_DATA(1),
      hps_io_hps_io_usb1_inst_D2     => HPS_USB_DATA(2),
      hps_io_hps_io_usb1_inst_D3     => HPS_USB_DATA(3),
      hps_io_hps_io_usb1_inst_D4     => HPS_USB_DATA(4),
      hps_io_hps_io_usb1_inst_D5     => HPS_USB_DATA(5),
      hps_io_hps_io_usb1_inst_D6     => HPS_USB_DATA(6),
      hps_io_hps_io_usb1_inst_D7     => HPS_USB_DATA(7),
      hps_io_hps_io_usb1_inst_CLK    => HPS_USB_CLKOUT,
      hps_io_hps_io_usb1_inst_STP    => HPS_USB_STP,
      hps_io_hps_io_usb1_inst_DIR    => HPS_USB_DIR,
      hps_io_hps_io_usb1_inst_NXT    => HPS_USB_NXT,
      hps_io_hps_io_gpio_inst_GPIO09 => HPS_CONV_USB_N,

      -- SPI
      hps_io_hps_io_spim1_inst_CLK  => HPS_SPIM_CLK,
      hps_io_hps_io_spim1_inst_MOSI => HPS_SPIM_MOSI,
      hps_io_hps_io_spim1_inst_MISO => HPS_SPIM_MISO,
      hps_io_hps_io_spim1_inst_SS0  => HPS_SPIM_SS,

      -- UART ADAPTER
      hps_io_hps_io_uart0_inst_RX => HPS_UART_RX,
      hps_io_hps_io_uart0_inst_TX => HPS_UART_TX,

      -- GPIO
      hps_io_hps_io_gpio_inst_LOANIO54 => HPS_KEY,
      hps_io_hps_io_gpio_inst_LOANIO53 => HPS_LED,
      hps_io_hps_io_gpio_inst_GPIO40   => HPS_LTC_GPIO,
      hps_io_hps_io_gpio_inst_GPIO61   => HPS_GSENSOR_INT);

end architecture Inst;
