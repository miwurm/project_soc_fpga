-------------------------------------------------------------------------------
--! @file      blinkylight_led_seq_pkg.vhd
--! @author    Michael Wurm <wurm.michael95@gmail.com>
--! @copyright 2017-2019 Michael Wurm
--! @brief     BlinkyLight LED test sequence.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library blinkylightlib;
use blinkylightlib.blinkylight_pkg.all;

library testbenchlib;
use testbenchlib.blinkylight_uvvm_pkg.all;
use testbenchlib.blinkylight_reg_pkg.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axilite;
use bitvis_vip_axilite.vvc_methods_pkg.all;
use bitvis_vip_axilite.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_axilite.td_target_support_pkg.all;

library bitvis_vip_gpio;
use bitvis_vip_gpio.vvc_methods_pkg.all;
use bitvis_vip_gpio.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_gpio.td_target_support_pkg.all;

--! @brief Package declaration of blinkylight_led_seq_pkg
--! @details
--! The BlinkyLight LED test sequence.

package blinkylight_led_seq_pkg is
  -----------------------------------------------------------------------------
  --! @name Types and Constants
  -----------------------------------------------------------------------------
  --! @{

  type dimmvalues_t is array(0 to 6) of positive;
  subtype led_port_t is std_ulogic_vector(num_of_leds_c-1 downto 0);

  constant dimmvalues_c : dimmvalues_t := (
    0 => 26,                            --  5%
    1 => 51,                            -- 10%
    2 => 102,                           -- 20%
    3 => 204,                           -- 40%
    4 => 256,                           -- 50%
    5 => 383,                           -- 75%
    6 => 501);                          -- 98%

  constant dimm_max_val_c : positive := to_integer(unsigned(BL_LED_DIMMVALUE0_MASK));
  constant dimm_period_c  : time     := led_count_inc_period_c * dimm_max_val_c;

  constant num_of_tests_c : positive := 5;

  --! @}
  -----------------------------------------------------------------------------
  -- Procedures
  -----------------------------------------------------------------------------
  --! @{

  procedure blinkylight_led_seq (
    signal start_i           : in    boolean;
    signal axi_vvc_i         : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal leds_gpio_vvc_i   : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural);

  procedure check_led_on_off (
    signal axi_vvc_i         : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal leds_gpio_vvc_i   : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural);

  procedure check_led_freq (
    constant dimm_val_g      : in    integer;
    constant dimm_val_max_g  : in    integer;
    constant period_g        : in    time;
    constant num_periods_g   : in    positive;
    signal axi_vvc_i         : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal leds_gpio_vvc_i   : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural);

  --! @}

end package blinkylight_led_seq_pkg;


package body blinkylight_led_seq_pkg is

  procedure blinkylight_led_seq (
    signal start_i           : in    boolean;
    signal axi_vvc_i         : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal leds_gpio_vvc_i   : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural) is
  begin

    await_value(start_i, true, 0 ns, 10 ns, error, "Wait for LED_SEQ to enable start.", TB_LED, ID_SEQUENCER);

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR_LARGE, "Test LEDs ON/OFF.", TB_LED);
    check_led_on_off(axi_vvc_i, leds_gpio_vvc_i, gpio_vvc_inst_g);

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR_LARGE, "Test LEDs frequency.", TB_LED);

    for i in 0 to dimmvalues_c'length - 1 loop
      check_led_freq(dimmvalues_c(i), dimm_max_val_c, dimm_period_c, num_of_tests_c, axi_vvc_i, leds_gpio_vvc_i, gpio_vvc_inst_g);
    end loop;

    await_completion(leds_gpio_vvc_i, gpio_vvc_inst_g, 10*dimm_period_c, "Waiting for LED_SEQ to complete.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Await completion.", TB_LED);
    await_completion(axi_vvc_i, 1, 200 ns, "Waiting for LED_SEQ to complete.");
    await_completion(leds_gpio_vvc_i, gpio_vvc_inst_g, 200 ns, "Waiting for LED_SEQ to complete.");
  end procedure blinkylight_led_seq;


  procedure check_led_on_off (
    signal axi_vvc_i         : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal leds_gpio_vvc_i   : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural) is

    variable value : std_ulogic_vector(31 downto 0);
  begin

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Set all LEDs to constant ON.", TB_LED);
    for i in 0 to num_of_leds_c-1 loop
      axilite_write(axi_vvc_i, 1, BL_LED_DIMMVALUE0_ADDR + i*4, x"FFFFFFFF", "Set LED " & integer'image(i) & " constant ON.");
    end loop;
    await_completion(axi_vvc_i, 1, num_of_leds_c * axi_access_time_c + 1 ns, "Waiting for LED registers to be written.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "LEDs should still be OFF.", TB_LED);
    gpio_check(leds_gpio_vvc_i, gpio_vvc_inst_g, (led_port_t'range => '0'), "LEDs are OFF.", error);
    await_completion(leds_gpio_vvc_i, gpio_vvc_inst_g, clk_period_c, "Check LEDs to be OFF.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Enable GUI Updates.", TB_LED);
    value := (BL_GUI_CONTROL_UPDATE_ENABLE_Pos => '1', others => '0');
    axilite_write(axi_vvc_i, 1, BL_GUI_CONTROL_ADDR, value, "Enable GUI updates.");
    await_completion(axi_vvc_i, 1, axi_access_time_c + 1 ns, "Waiting for GUI register to be written.");

    wait for num_of_leds_c * 2*clk_period_c;  -- for state machine to update LEDs

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "LEDs should now be ON.", TB_LED);
    gpio_check(leds_gpio_vvc_i, gpio_vvc_inst_g, (led_port_t'range => '1'), "LEDs are ON.", error);
    await_completion(leds_gpio_vvc_i, gpio_vvc_inst_g, clk_period_c, "Check LEDs to be ON.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Disable GUI Updates.", TB_LED);
    value := (BL_GUI_CONTROL_UPDATE_ENABLE_Pos => '0', others => '0');
    axilite_write(axi_vvc_i, 1, BL_GUI_CONTROL_ADDR, value, "Disable GUI updates.");
    await_completion(axi_vvc_i, 1, axi_access_time_c + 1 ns, "Waiting for GUI register to be written.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Set all LEDs to constant OFF.", TB_LED);
    for i in 0 to num_of_leds_c-1 loop
      axilite_write(axi_vvc_i, 1, BL_LED_DIMMVALUE0_ADDR + i*4, x"00000000", "Set LED " & integer'image(i) & " constant OFF.");
    end loop;
    await_completion(axi_vvc_i, 1, num_of_leds_c * axi_access_time_c + 1 ns, "Waiting for LED registers to be written.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "LEDs should still be ON.", TB_LED);
    gpio_check(leds_gpio_vvc_i, gpio_vvc_inst_g, (led_port_t'range => '1'), "LEDs are ON.", error);
    await_completion(leds_gpio_vvc_i, gpio_vvc_inst_g, clk_period_c, "Check LEDs to be ON.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Enable GUI Updates.", TB_LED);
    value := (BL_GUI_CONTROL_UPDATE_ENABLE_Pos => '1', others => '0');
    axilite_write(axi_vvc_i, 1, BL_GUI_CONTROL_ADDR, value, "Enable GUI updates.");
    await_completion(axi_vvc_i, 1, axi_access_time_c + 1 ns, "Waiting for GUI register to be written.");

    wait for num_of_leds_c * 2*clk_period_c;  -- for state machine to update LEDs

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "LEDs should now be OFF.", TB_LED);
    gpio_check(leds_gpio_vvc_i, gpio_vvc_inst_g, (led_port_t'range => '0'), "LEDs are OFF.", error);
    await_completion(leds_gpio_vvc_i, gpio_vvc_inst_g, clk_period_c, "Check LEDs to be OFF.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Disable GUI Updates.", TB_LED);
    value := (BL_GUI_CONTROL_UPDATE_ENABLE_Pos => '0', others => '0');
    axilite_write(axi_vvc_i, 1, BL_GUI_CONTROL_ADDR, value, "Disable GUI updates.");
    await_completion(axi_vvc_i, 1, axi_access_time_c + 1 ns, "Waiting for GUI register to be written.");
  end procedure check_led_on_off;


  procedure check_led_freq (
    constant dimm_val_g      : in    integer;
    constant dimm_val_max_g  : in    integer;
    constant period_g        : in    time;
    constant num_periods_g   : in    positive;
    signal axi_vvc_i         : inout bitvis_vip_axilite.td_target_support_pkg.t_vvc_target_record;
    signal leds_gpio_vvc_i   : inout bitvis_vip_gpio.td_target_support_pkg.t_vvc_target_record;
    constant gpio_vvc_inst_g : in    natural) is

    constant duty_cycle : real := real(dimm_val_g) / real(dimm_val_max_g);
    variable value      : std_ulogic_vector(31 downto 0);
  begin

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Enable GUI Updates.", TB_LED);
    value := (BL_GUI_CONTROL_UPDATE_ENABLE_Pos => '1', others => '0');
    axilite_write(axi_vvc_i, 1, BL_GUI_CONTROL_ADDR, value, "Enable GUI updates.");
    await_completion(axi_vvc_i, 1, axi_access_time_c + 1 ns, "Waiting for GUI register to be written.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Set LEDs to " & integer'image(integer(duty_cycle*100.0)) & "% dutycycle.", TB_LED);

    for i in 0 to num_of_leds_c-1 loop
      axilite_write(axi_vvc_i, 1, BL_LED_DIMMVALUE0_ADDR + i*4, std_ulogic_vector(to_unsigned(dimm_val_g, 32)), "Write LED" & integer'image(i) & " register.");
    end loop;
    await_completion(axi_vvc_i, 1, num_of_leds_c*axi_access_time_c + 5 ns, "Waiting for LED registers to be written.");

    ---------------------------------------------------------------------------
    log("Skip first period.", TB_LED);
    wait for period_g*4/5;

    gpio_expect(leds_gpio_vvc_i, gpio_vvc_inst_g, (led_port_t'range => '1'), period_g, "Wait to turn ON.");

    ---------------------------------------------------------------------------
    log("Check all ON/OFF periods.", TB_LED);
    for i in 0 to num_periods_g-1 loop
      gpio_expect(leds_gpio_vvc_i, gpio_vvc_inst_g, (led_port_t'range => '0'), duty_cycle * period_g + 150 ns, "Wait to turn OFF.");
      gpio_expect(leds_gpio_vvc_i, gpio_vvc_inst_g, (led_port_t'range => '1'), (1.0-duty_cycle) * period_g + 150 ns, "Wait to turn ON (next period).");
    end loop;
    await_completion(leds_gpio_vvc_i, gpio_vvc_inst_g, (num_periods_g + 1) * period_g, "Wait for ON/OFF check to complete.");

    ---------------------------------------------------------------------------
    log(ID_LOG_HDR, "Disable GUI Updates.", TB_LED);
    value := (BL_GUI_CONTROL_UPDATE_ENABLE_Pos => '0', others => '0');
    axilite_write(axi_vvc_i, 1, BL_GUI_CONTROL_ADDR, value, "Disable GUI updates.");
    await_completion(axi_vvc_i, 1, axi_access_time_c + 5 ns, "Waiting for GUI register to be written.");
  end procedure check_led_freq;

end package body blinkylight_led_seq_pkg;
