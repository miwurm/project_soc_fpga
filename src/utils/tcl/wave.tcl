#-------------------------------------------------------------------------------
# file:      wave.tcl
# author:    Michael Wurm <wurm.michael95@gmail.com>
# copyright: 2017-2019 Michael Wurm
# brief:     Generates waveform out of simulation log file.
#-------------------------------------------------------------------------------

eval transcript quietly

if [batch_mode] {
  eval echo "Detected batch mode"
  eval onerror {quit -f}
  eval onbreak {quit -f}
}

# Script parameters
set src_dir "../../src"

# Load arguments
set arg_log [lsearch $argv -Gopen_log=*]
if {$arg_log == -1} {
  eval echo "(ERROR) No log file specified."
  eval quit -f
}

set arg_tb [lsearch $argv -Gtestbench_top=*]
if {$arg_tb == -1} {
  eval echo "(ERROR) No testbench specified."
  eval quit -f
}

set testbench_top [string map {"-Gtestbench_top=" ""} [lindex $argv $arg_tb]]
set sim_top [format "%s/tb/%s_sim.tcl" $src_dir $testbench_top]
if {[file exists $sim_top]} {
  source $sim_top
} else {
  eval echo "(ERROR) The testbench <$testbench_top> is not yet implemented."
  eval quit -f
}

# Open log
set wlf_log [format "../../%s" [string map {"-Gopen_log=" ""} [lindex $argv $arg_log]]]
if {[file exists $wlf_log]} {
  eval vsim -view $wlf_log
} else {
  eval echo "(ERROR) Log file doesn't exist."
  eval quit -f
}

# Execute wave.do file if exists
set wavescript "../../build/sim/wave.do"
if {[file exists $wavescript]} {
  eval echo "(INFORMATION) using $wavescript"
  eval do wave.do
} else {
  # Generate waveform
  set wave_expand_param ""
  if {$wave_expand == 1} {
    set wave_expand_param "-expand"
  }
  source $src_dir/utils/tcl/wave_gen.tcl

  eval wave zoom full
}
eval configure wave -rowmargin $wave_row_margin -childrowmargin $wave_row_margin -timelineunits $wave_time_unit
