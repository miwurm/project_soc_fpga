#
# Author: Michael Wurm <wurm.michael95@gmail.com>
# Copyright (C) 2017-2019
#

# Source files in compilation order (top-down)
set tb_src {
  "ip/blinkylight/src/tb/blinkylight_reg_pkg.vhd"
  "tb/blinkylight_uvvm_pkg.vhd"
  "ip/blinkylight/src/tb/sequences/blinkylight_av_mm_reg_seq_pkg.vhd"
  "ip/blinkylight/src/tb/sequences/blinkylight_axi_reg_seq_pkg.vhd"
  "ip/blinkylight/src/tb/sequences/blinkylight_irq_seq_pkg.vhd"
  "ip/blinkylight/src/tb/sequences/blinkylight_led_seq_pkg.vhd"
  "ip/blinkylight/src/tb/sequences/blinkylight_sevseg_seq_pkg.vhd"
  "ip/blinkylight/src/tb/blinkylight_vvc_th.vhd"
  "ip/blinkylight/src/tb/blinkylight_vvc_tb.vhd"
}

# Simulation parameters
set work_lib "testbenchlib"
set design "blinkylight_vvc_tb"
set run_time "-all"
set time_unit "ps"
set sim_param ""
set generics ""

# Waveform parameters
set create_wave 1

# Add waves for objects in format {Object Recursive}
# Everything: {"${design}/*" 1}
set wave_patterns {
  {"${design}/*" 1}
}

set wave_expand 1
set wave_ignores {}
set wave_radix "hexadecimal"
set wave_time_unit "ns"
set wave_row_margin 8
set wave_zoom_range 0
set wave_zoom_start_time "0"
set wave_zoom_end_time "100"

# Coverage
set enable_coverage 1
set save_coverage 1
