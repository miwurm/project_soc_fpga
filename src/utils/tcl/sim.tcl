#-------------------------------------------------------------------------------
# file:      sim.tcl
# author:    Michael Wurm <wurm.michael95@gmail.com>
# copyright: 2017-2019 Michael Wurm
# brief:     Simulates testbench.
#-------------------------------------------------------------------------------

eval transcript quietly

if [batch_mode] {
  eval echo "Detected batch mode"
  eval onerror {quit -f}
  eval onbreak {quit -f}
}

# Script parameters
set src_dir "../../src"
set log_dir "../log"
set batch_mode_quit_at_end 1
set save_compile_times 0

set param ""
set vhdl_version "-2008"

# Load arguments
set arg_tb [lsearch $argv -Gtestbench_top=*]
if {$arg_tb == -1} {
  eval echo "(ERROR) No testbench specified."
  eval quit -f
}

set testbench_top [string map {"-Gtestbench_top=" ""} [lindex $argv $arg_tb]]
set sim_top [format "%s/tb/%s_sim.tcl" $src_dir $testbench_top]
if {[file exists $sim_top]} {
  source $sim_top
} else {
  eval echo "(ERROR) The testbench <$testbench_top> is not yet implemented."
  eval quit -f
}

# Create logging directory
if {[file exists $log_dir] == 0} {
  eval echo "Log directory \"$log_dir\" does not exist. Creating it."
  file mkdir $log_dir
}

# Create logging filenames
set start_timestamp [clock format [clock seconds] -format {%m/%d/%Y %I:%M:%S %p}]
set log_timestamp [clock format [clock seconds] -format {%Y%m%d_%H%M%S}]
set log_dir [format "%s/%s" $log_dir $log_timestamp]
file mkdir $log_dir

set sim_log_filename [format "%s/run_%s_sim.log"  $log_dir $log_timestamp]
set com_log_filename [format "%s/run_%s_com.log"  $log_dir $log_timestamp]
set wlf_log_filename [format "%s/run_%s_log.wlf"  $log_dir $log_timestamp]
set vcd_log_filename [format "%s/run_%s_log.vcd"  $log_dir $log_timestamp]
set cov_log_filename [format "%s/run_%s_cov.ucdb" $log_dir $log_timestamp]
set rpt_log_filename [format "%s/run_%s_cov.log"  $log_dir $log_timestamp]

# Set transcript file output
eval transcript file $com_log_filename

eval echo "\n======================================================================="
eval echo "Running simulation script ($start_timestamp)."
eval echo "======================================================================="

# Compile testbench
set quit_after_compile "false"
source $src_dir/utils/tcl/tb_compile.tcl

eval echo "\n-----------------------------------------------------------------------"
eval echo "Starting simulation."
eval echo "\n-----------------------------------------------------------------------"

if {[batch_mode]} {
  eval onerror resume
  eval onbreak resume
}

set vsim_param ""

set runtime [time [format "vsim %s -novopt -t %s -wlf %s -lib %s -l %s %s %s" $generics $time_unit $wlf_log_filename $work_lib $sim_log_filename $vsim_param $design]]
regexp {\d+} $runtime ct_microsecs
set ct_secs [expr {$ct_microsecs / 1000000.0}]
eval echo [format "Elaboration time: %.4f sec" $ct_secs]

# Generate waveform
if {$create_wave == 1} {
  set wave_expand_param ""
  if {$wave_expand == 1} {
    set wave_expand_param "-expand"
  }
  source $src_dir/utils/tcl/wave_gen.tcl
}

# Disable warnings
set StdArithNoWarnings 1
set NumericStdNoWarnings 1

# Run
set runtime [time [format "run %s" $run_time]]
regexp {\d+} $runtime ct_microsecs
set ct_secs [expr {$ct_microsecs / 1000000.0}]
eval echo [format "Simulation time: %s %s" $now $time_unit]
eval echo [format "Run time: %.4f sec" $ct_secs]

# Save coverage database
if {$enable_coverage == 1 && $save_coverage == 1} {
  eval coverage save $cov_log_filename
  eval coverage report -file $rpt_log_filename -instance /$design/test_harness/dut_*_inst -recursive -totals -assert -directive -cvg -codeAll
}

# Set wave time units and zoom
if {[batch_mode] == 0 && $create_wave == 1} {
  eval configure wave -rowmargin $wave_row_margin -childrowmargin $wave_row_margin -timelineunits $wave_time_unit
  if {$wave_zoom_range == 0} {
    eval wave zoom full
  } else {
    eval wave zoom range $wave_zoom_start_time $wave_zoom_end_time
    eval wave cursor time -time $wave_zoom_start_time
  }
}

# Quit
if {[batch_mode] && $batch_mode_quit_at_end == 1} {
  eval quit -f
}
