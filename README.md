# BlinkyLight FPGA Design

FPGA design for BlinkyLight, including a verification environment, using the Bitvis UVVM Library.

### Functionality
BlinkyLight's functionality is to variably dimm LEDs and to output characters on Sevensegment displays.  
The components are runtime-configurable through an AXI-Lite memory-mapped interface.

### Simulation and Verification
Bitvis UVVM Library is used to create an efficient testbench for the FPGA design.  
The project is tested with Altera ModelSim 10.5 and Mentor ModelSim 10.6b.

### How to run
This repository also focusses on creating a development environment that supports the developer to work efficiently.  
All functionalities this repository supports, are executed by the Makefile.

Make sure to check out submodules by running `git submodule update --init --recursive`.

`make help` is your friend.

### Toolchain
- Modelsim
- Python
- make

### Contact
Michael Wurm <<wurm.michael95@gmail.com>>
