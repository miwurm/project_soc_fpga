#!/bin/bash

FILE_ARG=${1:-none}
HDL_PRETTY_ARGS=--vhdl-basic-offset=2

if [ "$FILE_ARG" == "none" ]; then
  find src/ip/blinkylight/src/ -name '*.vhd' -print0 | while IFS= read -d '' -r file; do
    if [[ `echo "$file" | grep "blinkylight_axi.vhd"` ||
          `echo "$file" | grep "blinkylight_reg_pkg.vhd"` ||
          `echo "$file" | grep "blinkylight_av_mm.vhd"` ]]; then
      printf 'Skipped %s\n' "$file"
      continue
    fi
    printf 'Linting %s\n' "$file"
    ./hdl-pretty/vhdl-pretty $HDL_PRETTY_ARGS < "$file" > lint.vhd
    mv lint.vhd "$file"
  done

else # FILE_ARG was specified
    if [ ! -f $FILE_ARG ]; then
        echo "File not found:" "$FILE_ARG"
        exit 1
    fi
    printf 'Linting %s\n' "$FILE_ARG"
    ./hdl-pretty/vhdl-pretty $HDL_PRETTY_ARGS < "$FILE_ARG" > lint.vhd
    mv lint.vhd "$FILE_ARG"
fi
