/*
 * file:      blinkylight.c
 * author:    Michael Wurm <wurm.michael95@gmail.com>
 * copyright: 2018 Michael Wurm
 * brief:     Example Application for Project SoC FPGA.
 */

#include <stdint.h>

#include "blinkylight.h"

#define SOC BLINKYLIGHT_BASE

/* Variables */
static uint32_t counter = 0;

/* Returns logical values for 7seg displays */
uint32_t binTo7Seg(uint32_t val)
{
  switch(val)
  {
    case  0: return 0x3F;
    case  1: return 0x06;
    case  2: return 0x5B;
    case  3: return 0x4F;
    case  4: return 0x66;
    case  5: return 0x6D;
    case  6: return 0x7D;
    case  7: return 0x07;
    case  8: return 0x7F;
    case  9: return 0x6F;
    case 10: return 0x77;
    case 11: return 0x7C;
    case 12: return 0x39;
    case 13: return 0x5E;
    case 14: return 0x79;
    case 15: return 0x71;
    default: return 0x00;
  }
}

/* Busy delay */
void delay(uint32_t volatile time)
{
  while(time--);
}

/* Main function */
void main(void)
{
  int value = 0;
  while(1)
  {
    value = SOC->MAGIC_VALUE;
    if (value == 0x1234ABCD)
      SOC->LED_DIMMVALUE[4] = 0xFFFFFFFF;
    else
      SOC->LED_DIMMVALUE[4] = 0x0;

    SOC->LED_DIMMVALUE[2] = 0xFFFFFFFF;
  	delay(0x0003FFFF);
    SOC->LED_DIMMVALUE[2] = 0x00000000;
  	delay(0x0003FFFF);
  }
}
