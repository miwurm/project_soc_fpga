-------------------------------------------------------------------------------
--! @file      sevensegment.vhd
--! @author    Michael Wurm <wurm.michael95@gmail.com>
--! @copyright 2017-2019 Michael Wurm
--! @brief     Sevensegment implementation.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library blinkylightlib;
use blinkylightlib.blinkylight_pkg.all;

--! @brief Entity declaration of sevensegment
--! @details
--! Controls a Sevensegment Display.

entity sevensegment is
  port (
    --! @name Clocks and resets
    --! @{

    --! System clock
    clk_i   : in std_logic;
    --! Asynchronous reset
    rst_n_i : in std_logic;

    --! @}

    --! @name Sevensegment signals
    --! @{

    --! Sevensegment output
    sevseg_o : out sevseg_t;

    --! Enable updates
    enable_update_i : in std_ulogic;
    --! Blank sevensegment
    blank_i         : in std_ulogic;
    --! Character to display
    char_value_i    : in sevseg_char_t);

  --! @}

end entity sevensegment;

--! Runtime configurable RTL implementation of sevensegment
architecture rtl of sevensegment is
  -----------------------------------------------------------------------------
  --! @name Internal Registers
  -----------------------------------------------------------------------------
  --! @{

  signal char_value : sevseg_char_t;

  --! @}

begin  -- architecture rtl

  -----------------------------------------------------------------------------
  -- Outputs
  -----------------------------------------------------------------------------

  sevseg_o <= to_sevseg(std_ulogic_vector(char_value));

  -----------------------------------------------------------------------------
  -- Registers
  -----------------------------------------------------------------------------

  regs : process(clk_i, rst_n_i)
    procedure reset is
    begin
      char_value <= std_ulogic_vector(to_unsigned(sevseg_char_blank_c, char_value'length));
    end procedure reset;
  begin  -- process regs
    if rst_n_i = '0' then
      reset;
    elsif rising_edge(clk_i) then
      if enable_update_i = '1' then
        if blank_i = '1' then
          char_value <= std_ulogic_vector(to_unsigned(sevseg_char_blank_c, char_value'length));
        else
          char_value <= char_value_i;
        end if;
      end if;
    end if;
  end process regs;

end architecture rtl;
